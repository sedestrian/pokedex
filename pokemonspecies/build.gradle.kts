import AppDependencies.implementCoroutines
import AppDependencies.implementKoin
import AppDependencies.implementMoshi
import AppDependencies.implementRoom

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)

    defaultConfig {
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner(AppConfig.androidTestInstrumentation)
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(AppDependencies.appLibraries)
    implementation(AppDependencies.retrofit)

    implementMoshi()
    implementKoin()
    implementRoom()
    implementCoroutines()

    implementModule(AppDependencies.Modules.common)
    implementModule(AppDependencies.Modules.commonui)
    implementModule(AppDependencies.Modules.pokemon)
}