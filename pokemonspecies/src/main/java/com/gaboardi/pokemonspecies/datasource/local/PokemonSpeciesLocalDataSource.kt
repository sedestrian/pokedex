package com.gaboardi.pokemonspecies.datasource.local

import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies
import kotlinx.coroutines.flow.Flow

interface PokemonSpeciesLocalDataSource {
    fun observeSpecies(speciesId: Int): Flow<PokemonSpecies?>
    suspend fun insertSpecies(species: PokemonSpeciesResult)
}