package com.gaboardi.pokemonspecies.datasource.remote

import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult

interface PokemonSpeciesRemoteDataSource {
    suspend fun getPokemonSpecies(speciesId: Int): PokemonSpeciesResult
}