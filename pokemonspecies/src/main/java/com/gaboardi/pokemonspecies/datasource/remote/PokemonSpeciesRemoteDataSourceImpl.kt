package com.gaboardi.pokemonspecies.datasource.remote

import com.gaboardi.pokemonspecies.api.PokemonSpeciesApi
import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

class PokemonSpeciesRemoteDataSourceImpl(
    private val retrofit: Retrofit
) : PokemonSpeciesRemoteDataSource {
    override suspend fun getPokemonSpecies(speciesId: Int): PokemonSpeciesResult =
        withContext(Dispatchers.IO) {
            val api = retrofit.create(PokemonSpeciesApi::class.java)
            api.getPokemonSpecies(speciesId)
        }
}