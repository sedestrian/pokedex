package com.gaboardi.pokemonspecies.datasource.local

import com.gaboardi.pokemonspecies.converter.toInsertData
import com.gaboardi.pokemonspecies.converter.toUi
import com.gaboardi.pokemonspecies.dao.SpeciesDao
import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class PokemonSpeciesLocalDataSourceImpl(
    private val dao: SpeciesDao
) : PokemonSpeciesLocalDataSource {
    override fun observeSpecies(speciesId: Int) = dao.observeSpecies(speciesId)
        .distinctUntilChanged()
        .map { it?.toUi() }

    override suspend fun insertSpecies(species: PokemonSpeciesResult) =
        withContext(Dispatchers.IO) {
            dao.insertSpeciesData(species.toInsertData())
        }
}