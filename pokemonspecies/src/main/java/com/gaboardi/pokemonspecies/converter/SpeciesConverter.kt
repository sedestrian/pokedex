package com.gaboardi.pokemonspecies.converter

import android.net.Uri
import com.gaboardi.pokemon.converter.toUi
import com.gaboardi.pokemonspecies.model.api.FlavorTextResult
import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult
import com.gaboardi.pokemonspecies.model.db.FunFactEntity
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesData
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesEntity
import com.gaboardi.pokemonspecies.model.db.SpeciesInsertData
import com.gaboardi.pokemonspecies.model.ui.FunFact
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies

fun FlavorTextResult.toInsertData(speciesId: Int): FunFactEntity {
    return FunFactEntity(
        0,
        speciesId,
        flavor_text,
        language.name
    )
}

fun FunFactEntity.toUi(): FunFact {
    return FunFact(
        id,
        factText,
        language
    )
}

fun PokemonSpeciesResult.toInsertData(): SpeciesInsertData {
    val chainId = Uri.parse(evolution_chain.url).lastPathSegment?.toInt() ?: 1
    val varietyId =
        Uri.parse(varieties.first { it.is_default }.pokemon.url).lastPathSegment?.toInt() ?: 1
    return SpeciesInsertData(
        PokemonSpeciesEntity(
            id,
            name,
            base_happiness,
            chainId,
            varietyId
        ),
        flavor_text_entries.map { it.toInsertData(id) }
    )
}

fun PokemonSpeciesData.toUi(): PokemonSpecies {
    return PokemonSpecies(
        id,
        name,
        baseHappiness,
        evolutionChain,
        mainVarietyId,
        funFacts.map { it.toUi() },
        variety?.toUi()
    )
}