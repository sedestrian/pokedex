package com.gaboardi.pokemonspecies.repository

import com.gaboardi.common.model.State
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies
import kotlinx.coroutines.flow.Flow

interface SpeciesRepository {
    fun downloadSpecies(speciesId: Int): Flow<State>
    fun observeSpecies(speciesId: Int): Flow<PokemonSpecies?>
    fun downloadSpecies(speciesIds: List<Int>): Flow<State>
}