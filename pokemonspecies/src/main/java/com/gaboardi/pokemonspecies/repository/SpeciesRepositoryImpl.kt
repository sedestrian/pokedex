package com.gaboardi.pokemonspecies.repository

import android.net.Uri
import com.gaboardi.common.model.State
import com.gaboardi.pokemon.repository.PokemonRepository
import com.gaboardi.pokemonspecies.datasource.local.PokemonSpeciesLocalDataSource
import com.gaboardi.pokemonspecies.datasource.remote.PokemonSpeciesRemoteDataSource
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class SpeciesRepositoryImpl(
    private val remoteDataSource: PokemonSpeciesRemoteDataSource,
    private val localDataSource: PokemonSpeciesLocalDataSource,
    private val pokemonRepository: PokemonRepository
) : SpeciesRepository {
    override fun observeSpecies(speciesId: Int) = localDataSource.observeSpecies(speciesId)

    override fun downloadSpecies(speciesId: Int) = flow {
        emit(State.loading())
        try {
            downloadSpeciesInternal(speciesId)
            emit(State.success())
        } catch (e: Exception) {
            emit(State.error(e.localizedMessage))
        }
    }

    override fun downloadSpecies(speciesIds: List<Int>) = flow {
        emit(State.loading())
        try {
            coroutineScope {
                speciesIds.map { id ->
                    async { downloadSpeciesInternal(id, true) }
                }.map { deferred ->
                    deferred.await()
                }
                emit(State.success())
            }
        } catch (e: Exception) {
            emit(State.error(e.localizedMessage))
        }
    }

    private suspend fun downloadSpeciesInternal(speciesId: Int, downloadPokemon: Boolean = false) {
        val localSpecies = localDataSource.observeSpecies(speciesId).first()
        if (localSpecies != null) {
            if (downloadPokemon)
                pokemonRepository.downloadPokemon(localSpecies.mainVarietyId)
            return
        }

        val species = remoteDataSource.getPokemonSpecies(speciesId)
        localDataSource.insertSpecies(species)
        if (downloadPokemon) {
            val url = species.varieties.first { it.is_default }.pokemon.url
            val id = Uri.parse(url).lastPathSegment?.toInt() ?: 0
            pokemonRepository.downloadPokemon(id)
        }
    }
}