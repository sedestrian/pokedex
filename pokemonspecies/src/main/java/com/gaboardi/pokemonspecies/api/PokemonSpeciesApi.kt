package com.gaboardi.pokemonspecies.api

import com.gaboardi.pokemonspecies.model.api.PokemonSpeciesResult
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonSpeciesApi {
    @GET("pokemon-species/{id}")
    suspend fun getPokemonSpecies(@Path("id") speciesId: Int): PokemonSpeciesResult
}