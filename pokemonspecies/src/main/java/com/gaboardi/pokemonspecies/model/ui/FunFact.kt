package com.gaboardi.pokemonspecies.model.ui

data class FunFact(
    val id: Int,
    val factText: String,
    val language: String
)
