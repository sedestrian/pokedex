package com.gaboardi.pokemonspecies.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FlavorTextResult(
    val flavor_text: String,
    val language: FlavorTextLanguageResult
)
