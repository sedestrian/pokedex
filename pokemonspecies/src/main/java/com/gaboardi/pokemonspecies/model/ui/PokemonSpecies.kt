package com.gaboardi.pokemonspecies.model.ui

import com.gaboardi.pokemon.model.ui.Pokemon

data class PokemonSpecies(
    val id: Int,
    val name: String,
    val baseHappiness: Int,
    val evolutionChain: Int,
    val mainVarietyId: Int,
    val funFacts: List<FunFact>,
    val variety: Pokemon?
)
