package com.gaboardi.pokemonspecies.model.db

data class SpeciesInsertData(
    val species: PokemonSpeciesEntity,
    val facts: List<FunFactEntity>
)
