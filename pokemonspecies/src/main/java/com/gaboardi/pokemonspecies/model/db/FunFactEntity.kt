package com.gaboardi.pokemonspecies.model.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "funfacts",
    foreignKeys = [
        ForeignKey(
            entity = PokemonSpeciesEntity::class,
            parentColumns = ["id"],
            childColumns = ["speciesId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE,
            deferred = true
        )
    ]
)
data class FunFactEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val speciesId: Int,
    val factText: String,
    val language: String
)
