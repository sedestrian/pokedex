package com.gaboardi.pokemonspecies.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EvolutionChainUrlResult(
    val url: String
)
