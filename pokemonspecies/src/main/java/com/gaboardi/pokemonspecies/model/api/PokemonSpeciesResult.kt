package com.gaboardi.pokemonspecies.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonSpeciesResult(
    val id: Int,
    val name: String,
    val base_happiness: Int,
    val evolution_chain: EvolutionChainUrlResult,
    val flavor_text_entries: List<FlavorTextResult>,
    val varieties: List<VarietyResult>
)
