package com.gaboardi.pokemonspecies.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "species")
data class PokemonSpeciesEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val baseHappiness: Int,
    val evolutionChain: Int,
    val mainVarietyId: Int
)
