package com.gaboardi.pokemonspecies.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VarietyResult(
    val is_default: Boolean,
    val pokemon: VarietyPokemonResult
)