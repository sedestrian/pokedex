package com.gaboardi.pokemonspecies.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VarietyPokemonResult(
    val name: String,
    val url: String
)