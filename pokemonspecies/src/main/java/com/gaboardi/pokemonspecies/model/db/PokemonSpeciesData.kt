package com.gaboardi.pokemonspecies.model.db

import androidx.room.DatabaseView
import androidx.room.Relation
import com.gaboardi.pokemon.model.db.PokemonData

@DatabaseView(
    """
    SELECT 
        species.id,
        species.name,
        species.baseHappiness,
        species.evolutionChain,
        species.mainVarietyId,
        funfacts.*
    FROM 
        species 
    LEFT JOIN 
        funfacts 
    ON 
        species.id = funfacts.speciesId
    LEFT JOIN
        pokemon
    ON
        species.mainVarietyId = pokemon.id
    GROUP BY
        species.id
"""
)
data class PokemonSpeciesData(
    val id: Int,
    val name: String,
    val baseHappiness: Int,
    val evolutionChain: Int,
    val mainVarietyId: Int,
    @Relation(parentColumn = "id", entityColumn = "speciesId")
    val funFacts: List<FunFactEntity>,
    @Relation(parentColumn = "mainVarietyId", entityColumn = "id")
    val variety: PokemonData?
)