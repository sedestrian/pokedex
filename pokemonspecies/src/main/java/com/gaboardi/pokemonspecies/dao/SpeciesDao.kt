package com.gaboardi.pokemonspecies.dao

import androidx.room.*
import com.gaboardi.pokemonspecies.model.db.FunFactEntity
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesData
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesEntity
import com.gaboardi.pokemonspecies.model.db.SpeciesInsertData
import kotlinx.coroutines.flow.Flow

@Dao
abstract class SpeciesDao {
    @Transaction
    open fun insertSpeciesData(speciesInsertData: SpeciesInsertData){
        val species = speciesInsertData.species
        val facts = speciesInsertData.facts
        insertSpecies(species)
        insertFacts(facts)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSpecies(species: PokemonSpeciesEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSpecies(species: List<PokemonSpeciesEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertFacts(facts: List<FunFactEntity>)

    @Transaction
    @Query("SELECT * FROM species WHERE id = :speciesId LIMIT 1")
    abstract fun observeSpecies(speciesId: Int): Flow<PokemonSpeciesData?>
}