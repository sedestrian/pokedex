package com.gaboardi.pokemonspecies.di

import com.gaboardi.pokemonspecies.datasource.local.PokemonSpeciesLocalDataSource
import com.gaboardi.pokemonspecies.datasource.local.PokemonSpeciesLocalDataSourceImpl
import com.gaboardi.pokemonspecies.datasource.remote.PokemonSpeciesRemoteDataSource
import com.gaboardi.pokemonspecies.datasource.remote.PokemonSpeciesRemoteDataSourceImpl
import com.gaboardi.pokemonspecies.repository.SpeciesRepository
import com.gaboardi.pokemonspecies.repository.SpeciesRepositoryImpl
import org.koin.dsl.module

val speciesModule = module {
    single<PokemonSpeciesLocalDataSource> { PokemonSpeciesLocalDataSourceImpl(get()) }
    single<PokemonSpeciesRemoteDataSource> { PokemonSpeciesRemoteDataSourceImpl(get()) }
    single<SpeciesRepository> { SpeciesRepositoryImpl(get(), get(), get()) }
}