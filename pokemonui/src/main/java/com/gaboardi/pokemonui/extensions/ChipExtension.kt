package com.gaboardi.pokemonui.extensions

import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import com.gaboardi.commonui.R
import com.gaboardi.commonui.extensions.getColorAttribute
import com.gaboardi.commonui.extensions.isUsingNightModeResources
import com.gaboardi.pokemon.model.ui.PokemonType
import com.gaboardi.pokemon.model.ui.PokemonTypeColor
import com.google.android.material.chip.Chip
import java.util.*

fun Chip.setPokemonType(pokemonType: PokemonType, colorLightMode: Boolean = true) {
    val isNight = context.isUsingNightModeResources()
    val typeColors = PokemonTypeColor.fromType(pokemonType.name)
    if (isNight) {
        typeColors?.let {
            val background = ContextCompat.getColor(context, typeColors.background)
            val foreground = ContextCompat.getColor(context, typeColors.textColor)

            chipBackgroundColor = ColorStateList.valueOf(background)
            setTextColor(foreground)
        }
    } else {
        if (!colorLightMode) {
            val background = context.getColorAttribute(R.attr.colorSurface)
            val foreground = context.getColorAttribute(R.attr.colorOnSurface)
            chipBackgroundColor = ColorStateList.valueOf(background)
            setTextColor(foreground)
        } else {
            typeColors?.let {
                val background = ContextCompat.getColor(context, typeColors.background)
                val foreground = ContextCompat.getColor(context, typeColors.textColor)

                chipBackgroundColor = ColorStateList.valueOf(background)
                setTextColor(foreground)
            }
        }
    }
    text = pokemonType.name.capitalize(Locale.getDefault())
}
