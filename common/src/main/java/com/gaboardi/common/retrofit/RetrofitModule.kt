package com.gaboardi.common.retrofit

import org.koin.dsl.module

val retrofitModule = module {
    single { RetrofitProvider.initRetrofit() }
}