package com.gaboardi.common.model

import androidx.annotation.StringRes

sealed class State {
    object Loading : State()

    data class Success(val id: String? = null) : State()

    data class Error(val message: ErrorMessage) : State()

    companion object {

        /**
         * Returns [State.Loading] instance.
         */
        fun loading() = Loading

        /**
         * Returns [State.Success] instance.
         * @param id Identifier for inserted items.
         */
        fun success(id: String? = null) =
            Success(id)

        /**
         * Returns [State.Error] instance.
         * @param message Description of failure.
         */
        fun error(message: ErrorMessage) =
            Error(message)

        fun error(message: String, id: String? = null) = Error(ErrorMessage.of(message, id))
        fun error(@StringRes message: Int, id: String? = null) = Error(ErrorMessage.of(message, id))
    }

}