package com.gaboardi.commonui.extensions

import android.content.Context
import android.content.res.Configuration
import androidx.annotation.AttrRes
import androidx.core.content.ContextCompat
import com.gaboardi.commonui.R

fun Context.getColorAttribute(@AttrRes attribute: Int): Int {
    val default = ContextCompat.getColor(this, R.color.black)
    val attributes = this.theme.obtainStyledAttributes(intArrayOf(attribute))
    val color = attributes.getColor(0, default)
    attributes.recycle()
    return color
}

fun Context.isUsingNightModeResources(): Boolean {
    return when (resources.configuration.uiMode and
            Configuration.UI_MODE_NIGHT_MASK) {
        Configuration.UI_MODE_NIGHT_YES -> true
        Configuration.UI_MODE_NIGHT_NO -> false
        Configuration.UI_MODE_NIGHT_UNDEFINED -> false
        else -> false
    }
}