package com.gaboardi.commonui.itemdecoration

import android.graphics.Rect
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.dp

class LinearSpacingItemDecorator(private val horizontal: Int = 16.dp, private val vertical: Int = 16.dp, val direction: Int = LinearLayoutManager.VERTICAL) :
    RecyclerView.ItemDecoration() {
    /**
     * Metodo che aggiunge margine tra gli elementi di una RecyclerView
     */
    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        if(direction == LinearLayoutManager.VERTICAL) {
            outRect.right = horizontal / 2
            outRect.left = horizontal / 2

            outRect.top = if (itemPosition == 0)
                vertical
            else
                vertical / 2

            outRect.bottom = parent.adapter?.itemCount?.let {
                if (itemPosition == it - 1)
                    vertical
                else
                    vertical / 2
            } ?: vertical / 2
        }else{
            outRect.top = vertical / 2
            outRect.bottom = vertical / 2

            outRect.left = if(itemPosition == 0)
                horizontal
            else
                horizontal / 2

            outRect.right = parent.adapter?.itemCount?.let {
                if(itemPosition == it - 1)
                    horizontal
                else
                    horizontal / 2
            } ?: horizontal / 2
        }
    }
}