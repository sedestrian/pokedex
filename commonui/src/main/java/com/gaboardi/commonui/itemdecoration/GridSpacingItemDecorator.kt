package com.gaboardi.commonui.itemdecoration

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.common.extensions.dp

class GridSpacingItemDecorator(
    private val columns: Int,
    private val horizontal: Int = 16.dp,
    private val vertical: Int = 16.dp
) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        val column: Int = itemPosition % columns

        outRect.left = horizontal - column * horizontal / columns
        outRect.right = (column + 1) * horizontal / columns
        if (itemPosition < columns) {
            outRect.top = vertical
        }
        outRect.bottom = vertical
    }
}