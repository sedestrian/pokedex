import AppDependencies.implementGlide
import AppDependencies.implementKoin
import AppDependencies.implementMoshi
import AppDependencies.implementRoom

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
}

android {
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)

    defaultConfig {
        applicationId = AppConfig.appId
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility(JavaVersion.VERSION_1_8)
        targetCompatibility(JavaVersion.VERSION_1_8)
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(AppDependencies.appLibraries)
    implementation(AppDependencies.paging)
    implementBom(AppDependencies.firebaseBom)
    implementation(AppDependencies.crashlitics)

    implementMoshi()
    implementKoin()
    implementGlide()
    implementRoom()

    implementModules(AppDependencies.Modules.all)
}

repositories{
    maven {
        url = uri("https://jitpack.io")
    }
}