package com.gaboardi.pokedex

import android.app.Application
import androidx.paging.ExperimentalPagingApi
import com.gaboardi.common.retrofit.retrofitModule
import com.gaboardi.evolutionchain.di.evolutionModule
import com.gaboardi.pokedex.di.roomModule
import com.gaboardi.pokemon.di.pokemonModule
import com.gaboardi.pokemondetail.di.pokemonDetailModule
import com.gaboardi.pokemonlist.di.pokemonListModule
import com.gaboardi.pokemonspecies.di.speciesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PokeApplication : Application() {
    @ExperimentalPagingApi
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            modules(
                roomModule,
                retrofitModule,
                pokemonModule,
                pokemonListModule,
                pokemonDetailModule,
                speciesModule,
                evolutionModule
            )
        }
    }
}