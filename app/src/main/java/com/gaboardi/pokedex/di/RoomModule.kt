package com.gaboardi.pokedex.di

import androidx.room.Room
import com.gaboardi.pokedex.db.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val roomModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java,
            androidContext().packageName
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<AppDatabase>().pokemonDao() }
    single { get<AppDatabase>().speciesDao() }
    single { get<AppDatabase>().evolutionDao() }
}