package com.gaboardi.pokedex.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gaboardi.evolutionchain.dao.EvolutionDao
import com.gaboardi.evolutionchain.model.db.EvolutionEntity
import com.gaboardi.pokemon.dao.PokemonDao
import com.gaboardi.pokemon.model.db.*
import com.gaboardi.pokemonspecies.dao.SpeciesDao
import com.gaboardi.pokemonspecies.model.db.FunFactEntity
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesData
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesEntity

@Database(
    entities = [
        RemoteKeyEntity::class,
        PokemonEntity::class,
        TypeEntity::class,
        StatEntity::class,
        PokemonSpeciesEntity::class,
        FunFactEntity::class,
        EvolutionEntity::class,
    ],
    views = [
        PokemonSpeciesData::class,
        PokemonData::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
    abstract fun speciesDao(): SpeciesDao
    abstract fun evolutionDao(): EvolutionDao
}