//version constants for the Kotlin DSL dependencies
object Versions {
    //app level
    const val crashlitics = "2.4.1"
    const val google = "4.3.4"
    const val gradle = "4.1.2"
    const val kotlin = "1.4.30-RC"

    //libs
    const val appcompat = "1.3.0-beta01"
    const val constraintLayout = "2.0.0-beta8"
    const val coreKtx = "1.5.0-beta01"
    const val coroutines = "1.4.0"
    const val firebase = "26.3.0"
    const val glide = "4.11.0"
    const val koin = "2.2.0"
    const val lottie = "3.6.0"
    const val material = "1.3.0-beta01"
    const val moshi = "1.11.0"
    const val navigation = "2.3.3"
    const val orbit = "2.2.0"
    const val paging = "3.0.0-alpha13"
    const val retrofit = "2.9.0"
    const val retrofitLogging = "4.9.0"
    const val room = "2.3.0-beta01"
    const val recycler = "1.2.0-alpha06"
    const val shimmer = "0.5.0"
    const val svgGlide = "2.0.4"
    const val swipeRefresh = "1.1.0"
}