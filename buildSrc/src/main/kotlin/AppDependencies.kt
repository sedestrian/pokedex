import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.project

object AppDependencies {
    //region Kotlin
    private const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    //endregion

    //region Base libs
    private const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    private const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    //endregion

    //region Koin
    private const val koin = "org.koin:koin-core:${Versions.koin}"
    private const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    private const val koinFragment = "org.koin:koin-androidx-fragment:${Versions.koin}"
    //endregion

    //region UI
    const val material = "com.google.android.material:material:${Versions.material}"
    const val swipeRefresh = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefresh}"
    private const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recycler}"
    const val lottie = "com.airbnb.android:lottie:${Versions.lottie}"
    const val shimmer = "com.facebook.shimmer:shimmer:${Versions.shimmer}"

    private const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    private const val glideProcessor = "com.github.bumptech.glide:compiler:${Versions.glide}"
    private const val svgGlide = "com.github.qoqa:glide-svg:${Versions.svgGlide}"
    //endregion

    //region Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitLogging =
        "com.squareup.okhttp3:logging-interceptor:${Versions.retrofitLogging}"
    const val moshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    //endregion

    //region Paging
    const val paging = "androidx.paging:paging-runtime-ktx:${Versions.paging}"
    //endregion

    //region Firebase
    const val firebaseBom = "com.google.firebase:firebase-bom:${Versions.firebase}"
    const val crashlitics = "com.google.firebase:firebase-crashlytics-ktx"
    //endregion

    //region Moshi
    private const val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    private const val moshiKotlin = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    //endregion

    //region Coroutines
    private const val coroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    private const val coroutinesAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    //endregion

    //region MVI
    private const val orbit = "org.orbit-mvi:orbit-core:${Versions.orbit}"
    private const val orbitViewModel = "org.orbit-mvi:orbit-viewmodel:${Versions.orbit}"
    private const val orbitCoroutines = "org.orbit-mvi:orbit-coroutines:${Versions.orbit}"
    //endregion

    //region Room
    private const val room = "androidx.room:room-runtime:${Versions.room}"
    private const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    private const val roomProcessor = "androidx.room:room-compiler:${Versions.room}"
    //endregion

    //region Navigation
    private const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    private const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    //endregion

    object Modules {
        const val app = ":app"
        const val common = ":common"
        const val commonui = ":commonui"
        const val host = ":host"
        const val navigation = ":navigation"
        const val pokemonList = ":pokemonlist"
        const val pokemon = ":pokemon"
        const val pokemonui = ":pokemonui"
        const val pokemonDetail = ":pokemondetail"
        const val pokemonSpecies = ":pokemonspecies"
        const val evolutionChain = ":evolutionchain"

        val all = arrayListOf<String>().apply {
            add(common)
            add(commonui)
            add(host)
            add(navigation)
            add(pokemonList)
            add(pokemon)
            add(pokemonui)
            add(pokemonDetail)
            add(pokemonSpecies)
            add(evolutionChain)
        }
    }

    val appLibraries = arrayListOf<String>().apply {
        add(kotlinStdLib)
        add(coreKtx)
        add(appcompat)
        add(constraintLayout)
    }

    val navigation = arrayListOf<String>().apply {
        add(navigationFragment)
        add(navigationUi)
    }

    fun DependencyHandler.implementKoin() {
        add(Implementation, koin)
        add(Implementation, koinViewModel)
        add(Implementation, koinFragment)
    }

    fun DependencyHandler.implementGlide() {
        add(Implementation, svgGlide)
        add(Implementation, glide)
        add(Kapt, glideProcessor)
    }

    fun DependencyHandler.implementRoom() {
        add(Implementation, room)
        add(Implementation, roomKtx)
        add(Kapt, roomProcessor)
    }

    fun DependencyHandler.implementOrbit() {
        add(Implementation, orbit)
        add(Implementation, orbitCoroutines)
        add(Implementation, orbitViewModel)
    }

    fun DependencyHandler.implementCoroutines() {
        add(Implementation, coroutines)
        add(Implementation, coroutinesAndroid)
    }

    fun DependencyHandler.implementMoshi() {
        add(Implementation, moshi)
        add(Kapt, moshiKotlin)
    }
}

private const val Implementation = "implementation"
private const val Kapt = "kapt"

//util functions for adding the different type dependencies from build.gradle file
fun DependencyHandler.implementModules(list: List<String>) {
    list.forEach { dependency ->
        add(Implementation, project(dependency))
    }
}

fun DependencyHandler.implementBom(dependency: String) {
    add(Implementation, platform(dependency))
}

fun DependencyHandler.implementModule(module: String) {
    add(Implementation, project(module))
}

fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add(Kapt, dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add(Implementation, dependency)
    }
}