plugins {
    id("com.gradle.enterprise").version("3.5.1")
}

gradleEnterprise {
    buildScan {
        termsOfServiceUrl = "https://gradle.com/terms-of-service"
        termsOfServiceAgree = "yes"
        publishAlways()
    }
}

rootProject.name = "PokéDex"

include (":app", ":common", ":commonui", ":navigation", ":host")
include(":pokemonlist")
include(":pokemon")
include(":pokemondetail")
include(":pokemonspecies")
include(":evolutionchain")
include(":pokemonui")
