package com.gaboardi.pokemon.datasource.local

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.gaboardi.pokemon.dao.PokemonDao
import com.gaboardi.pokemon.model.db.PokemonData
import kotlin.math.roundToInt

class PokemonPagingSource(
    private val roomPagingSource: PagingSource<Int, PokemonData>,
    private val pokemonDao: PokemonDao,
    private val pageSize: Int,
) : PagingSource<Int, PokemonData>() {
    init {
        roomPagingSource.registerInvalidatedCallback {
            invalidate()
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PokemonData> =
        when (params) {
            is LoadParams.Refresh -> calculateRefresh(params)
            is LoadParams.Append -> calculateAppend(params)
            is LoadParams.Prepend -> calculatePrepend(params)
        }

    private suspend fun calculateRefresh(params: LoadParams<Int>): LoadResult<Int, PokemonData> {
        val key = params.key ?: 0
        val actualKey = (key.toFloat() / pageSize).roundToInt() * pageSize
        val beforeKey = pokemonDao.getItemsBefore(actualKey)
        val afterKey = pokemonDao.getItemsAfter(actualKey)
        val end = if (afterKey >= pageSize) {
            if (beforeKey >= pageSize)
                actualKey + pageSize
            else
                actualKey + afterKey.coerceAtMost(params.loadSize - pageSize)
        } else {
            actualKey
        }
        val start = if (beforeKey >= pageSize) {
            if (afterKey >= pageSize)
                actualKey - pageSize
            else
                actualKey - beforeKey.coerceAtMost(params.loadSize - pageSize)
        } else {
            actualKey
        }
        val pokemon = pokemonDao.getPokemonPage(start = start, end = end)
        val before = pokemonDao.getItemsBefore(start)
        val after = pokemonDao.getItemsAfter(end)
        val prevKey =
            if (pokemon.size < params.loadSize || before == 0) null else pokemon.firstOrNull()?.key?.currentPage
        val nextKey =
            if (pokemon.size < params.loadSize || after == 0) null else pokemon.lastOrNull()?.key?.nextPage
        return LoadResult.Page(pokemon, if (prevKey == 0) null else prevKey, nextKey, before, after)
    }

    private suspend fun calculateAppend(params: LoadParams<Int>): LoadResult<Int, PokemonData> {
        val key = params.key ?: 0
        val pokemon = pokemonDao.getPokemonPage(key, key)
        val before = Int.MIN_VALUE
        val after = Int.MIN_VALUE
        val nextKey = pokemon.lastOrNull()?.key?.nextPage
        val prevKey = key
        return LoadResult.Page(pokemon, prevKey, nextKey, before, after)
    }

    private suspend fun calculatePrepend(params: LoadParams<Int>): LoadResult<Int, PokemonData> {
        val key = ((params.key ?: 0) - pageSize).coerceAtLeast(0)
        val pokemon = pokemonDao.getPokemonPage(key, key)
        val before = Int.MIN_VALUE
        val after = Int.MIN_VALUE
        val nextKey = pokemon.lastOrNull()?.key?.nextPage
        val prevKey = if (key == 0) null else key
        return LoadResult.Page(pokemon, prevKey, nextKey, before, after)
    }

    override fun getRefreshKey(state: PagingState<Int, PokemonData>): Int? =
        roomPagingSource.getRefreshKey(state)
}