package com.gaboardi.pokemon.datasource.remote

import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.api.pokemonlist.PokemonListContainer

interface PokemonRemoteDataSource {
    suspend fun getPokemonPage(offset: Int, limit: Int): PokemonListContainer
    suspend fun getPokemonData(pokemonId: Int): PokemonDetailsResult
}