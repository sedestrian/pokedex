package com.gaboardi.pokemon.datasource.local

import androidx.paging.PagingSource
import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.db.PokemonData
import com.gaboardi.pokemon.model.db.RemoteKeyEntity
import com.gaboardi.pokemon.model.ui.Pokemon
import kotlinx.coroutines.flow.Flow

interface PokemonLocalDataSource {
    suspend fun insertPokemon(pokemon: List<PokemonDetailsResult>)
    fun observePokemonPaging(pageSize: Int): PagingSource<Int, PokemonData>
    fun observePokemon(pokemonId: Int): Flow<Pokemon?>
    suspend fun insertPokemon(pokemon: PokemonDetailsResult)
    suspend fun insertKeys(keys: List<RemoteKeyEntity>)
    suspend fun getKeyFor(id: Int): RemoteKeyEntity?
}