package com.gaboardi.pokemon.datasource.local

import androidx.paging.PagingSource
import com.gaboardi.pokemon.converter.toInsertData
import com.gaboardi.pokemon.converter.toUi
import com.gaboardi.pokemon.dao.PokemonDao
import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.db.PokemonData
import com.gaboardi.pokemon.model.db.RemoteKeyEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class PokemonLocalDataSourceImpl(
    private val dao: PokemonDao
) : PokemonLocalDataSource {
    override fun observePokemonPaging(pageSize: Int): PagingSource<Int, PokemonData> =
        PokemonPagingSource(dao.observePokemon(), dao, pageSize)

    override fun observePokemon(pokemonId: Int) = dao.observePokemon(pokemonId)
        .distinctUntilChanged()
        .map { it?.toUi() }

    override suspend fun insertPokemon(pokemon: List<PokemonDetailsResult>) =
        withContext(Dispatchers.IO) {
            dao.insertPokemon(pokemon.map { it.toInsertData() })
        }

    override suspend fun insertPokemon(pokemon: PokemonDetailsResult) =
        withContext(Dispatchers.IO) {
            dao.insertPokemon(pokemon.toInsertData())
        }

    override suspend fun insertKeys(keys: List<RemoteKeyEntity>) = withContext(Dispatchers.IO) {
        dao.insertKeys(keys)
    }

    override suspend fun getKeyFor(id: Int) = withContext(Dispatchers.IO) { dao.getKeyFor(id) }
}