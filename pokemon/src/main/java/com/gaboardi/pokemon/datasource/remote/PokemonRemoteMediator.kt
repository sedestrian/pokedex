package com.gaboardi.pokemon.datasource.remote

import android.net.Uri
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.gaboardi.pokemon.datasource.local.PokemonLocalDataSource
import com.gaboardi.pokemon.model.db.PokemonData
import com.gaboardi.pokemon.model.db.RemoteKeyEntity
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

@ExperimentalPagingApi
class PokemonRemoteMediator(
    private val remoteDataSource: PokemonRemoteDataSource,
    private val localDataSource: PokemonLocalDataSource,
) : RemoteMediator<Int, PokemonData>() {
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, PokemonData>
    ): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> {
                state.anchorPosition?.let { anchor ->
                    state.closestItemToPosition(anchor)?.key?.currentPage ?: 0
                } ?: 0
            }
            LoadType.PREPEND -> {
                val firstPage = state.pages.firstOrNull { it.data.isNotEmpty() }
                val key = firstPage?.data?.firstOrNull()?.key?.previousPage
                key ?: return MediatorResult.Success(endOfPaginationReached = true)
            }
            LoadType.APPEND -> {
                val lastPage = state.pages.lastOrNull { it.data.isNotEmpty() }
                val key = lastPage?.data?.lastOrNull()?.key?.nextPage
                key ?: return MediatorResult.Success(endOfPaginationReached = true)
            }
        }

        return try {
            coroutineScope {
                val result = remoteDataSource.getPokemonPage(
                    page, state.config.pageSize
                )
                val pokemon = result.results.map {
                    val id = Uri.parse(it.url).lastPathSegment?.toInt() ?: 0
                    async { remoteDataSource.getPokemonData(id) }
                }.map { it.await() }

                localDataSource.insertPokemon(pokemon)

                val next = Uri.parse(result.next)?.getQueryParameter("offset")?.toInt()
                val previous =
                    if (result.previous.isNullOrBlank()) null else Uri.parse(result.previous)
                        ?.getQueryParameter("offset")?.toInt()
                val keys =
                    pokemon.map { RemoteKeyEntity(it.id, previous, page, next) }
                localDataSource.insertKeys(keys)

                MediatorResult.Success(endOfPaginationReached = result.next.isNullOrBlank())
            }
        } catch (e: Exception) {
            MediatorResult.Error(e)
        }
    }
}