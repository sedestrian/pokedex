package com.gaboardi.pokemon.datasource.remote

import com.gaboardi.pokemon.api.PokemonApi
import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.api.pokemonlist.PokemonListContainer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

class PokemonRemoteDataSourceImpl(
    private val retrofit: Retrofit
) : PokemonRemoteDataSource {
    override suspend fun getPokemonPage(offset: Int, limit: Int): PokemonListContainer =
        withContext(Dispatchers.IO) {
            val api = retrofit.create(PokemonApi::class.java)
            api.pokemonList(offset, limit)
        }

    override suspend fun getPokemonData(pokemonId: Int): PokemonDetailsResult =
        withContext(Dispatchers.IO) {
            val api = retrofit.create(PokemonApi::class.java)
            api.getPokemonDetails(pokemonId)
        }
}