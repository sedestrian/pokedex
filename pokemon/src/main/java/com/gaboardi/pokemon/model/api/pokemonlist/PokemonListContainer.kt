package com.gaboardi.pokemon.model.api.pokemonlist

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonListContainer(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<PokemonListResult>
)