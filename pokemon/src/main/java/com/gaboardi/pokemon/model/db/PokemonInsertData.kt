package com.gaboardi.pokemon.model.db

data class PokemonInsertData(
    val pokemon: PokemonEntity,
    val pokemonStats: List<StatEntity>,
    val pokemonTypes: List<TypeEntity>
)