package com.gaboardi.pokemon.model.api.pokemonlist

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonListResult(
    val name: String,
    val url: String
)