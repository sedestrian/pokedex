package com.gaboardi.pokemon.model.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "keys",
    foreignKeys = [
        ForeignKey(
            entity = PokemonEntity::class,
            parentColumns = ["id"],
            childColumns = ["pokemonId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE,
            deferred = true
        )
    ]
)
data class RemoteKeyEntity(
    @PrimaryKey
    val pokemonId: Int,
    val previousPage: Int?,
    val currentPage: Int?,
    val nextPage: Int?
)