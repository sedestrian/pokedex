package com.gaboardi.pokemon.model.ui

import androidx.annotation.ColorRes
import com.gaboardi.commonui.R

enum class PokemonColor(
    val types: List<PokemonTypeEnum>,
    @ColorRes val background: Int,
    @ColorRes val foreground: Int
) {
    GREEN(
        listOf(PokemonTypeEnum.Grass, PokemonTypeEnum.Bug, PokemonTypeEnum.Fairy),
        R.color.green_200,
        R.color.green_500
    ),
    RED(listOf(PokemonTypeEnum.Fire), R.color.red_200, R.color.red_500),
    BLUE(
        listOf(
            PokemonTypeEnum.Water,
            PokemonTypeEnum.Ice,
            PokemonTypeEnum.Fighting,
            PokemonTypeEnum.Normal
        ), R.color.blue_200, R.color.blue_500
    ),
    YELLOW(
        listOf(PokemonTypeEnum.Electric, PokemonTypeEnum.Psychic),
        R.color.yellow_200,
        R.color.yellow_500
    ),
    PURPLE(
        listOf(PokemonTypeEnum.Poison, PokemonTypeEnum.Ghost),
        R.color.purple_200,
        R.color.purple_500
    ),
    BROWN(
        listOf(PokemonTypeEnum.Ground, PokemonTypeEnum.Rock),
        R.color.brown_200,
        R.color.brown_500
    ),
    BLACK(
        listOf(PokemonTypeEnum.Dark, PokemonTypeEnum.Unknown, PokemonTypeEnum.Dragon),
        R.color.black_200,
        R.color.black
    );

    companion object {
        fun from(type: String?): PokemonColor? =
            values().firstOrNull { value -> value.types.any { typeEnum -> typeEnum.id == type } }

        fun from(type: PokemonTypeEnum): PokemonColor? =
            values().firstOrNull { value -> value.types.any { typeEnum -> typeEnum == type } }
    }
}