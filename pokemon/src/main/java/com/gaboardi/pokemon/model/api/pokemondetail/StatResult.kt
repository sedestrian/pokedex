package com.gaboardi.pokemon.model.api.pokemondetail

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StatResult(
    val base_stat: Int,
    val effort: Int,
    val stat: StatDetailResult
)