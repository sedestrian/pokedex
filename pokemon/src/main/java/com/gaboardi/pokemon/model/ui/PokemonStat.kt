package com.gaboardi.pokemon.model.ui

import androidx.recyclerview.widget.DiffUtil

data class PokemonStat(
    val baseStat: Int,
    val effort: Int,
    val name: String
) {
    data class ChangePayload(
        val name: String?,
        val baseStat: Int?
    )

    companion object {
        val DiffUtil = object : DiffUtil.ItemCallback<PokemonStat>() {
            override fun areItemsTheSame(oldItem: PokemonStat, newItem: PokemonStat): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: PokemonStat, newItem: PokemonStat): Boolean {
                return oldItem.name == newItem.name && oldItem.baseStat == newItem.baseStat
            }

            override fun getChangePayload(oldItem: PokemonStat, newItem: PokemonStat): Any {
                return ChangePayload(
                    if (oldItem.name == newItem.name) null else newItem.name,
                    if (oldItem.baseStat == newItem.baseStat) null else newItem.baseStat
                )
            }
        }
    }
}