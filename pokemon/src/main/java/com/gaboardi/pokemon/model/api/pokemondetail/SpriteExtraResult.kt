package com.gaboardi.pokemon.model.api.pokemondetail

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SpriteExtraResult(
    val dream_world: DreamWorldSpriteResult,
    @Json(name = "official-artwork")
    val officialArtwork: OfficialArtworkResult
)