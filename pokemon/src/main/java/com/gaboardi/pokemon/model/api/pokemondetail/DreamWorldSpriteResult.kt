package com.gaboardi.pokemon.model.api.pokemondetail

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DreamWorldSpriteResult(
    val front_default: String?,
    val front_female: String?
)