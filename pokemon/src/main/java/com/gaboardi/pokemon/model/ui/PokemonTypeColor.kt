package com.gaboardi.pokemon.model.ui

import androidx.annotation.ColorRes
import com.gaboardi.pokemon.R

enum class PokemonTypeColor(
    val type: PokemonTypeEnum,
    @ColorRes val background: Int,
    @ColorRes val textColor: Int
) {
    Normal(PokemonTypeEnum.Normal, R.color.normal_type_background, R.color.normal_type_foreground),
    Fighting(PokemonTypeEnum.Fighting, R.color.fighting_type_background, R.color.fighting_type_foreground),
    Flying(PokemonTypeEnum.Flying, R.color.flying_type_background, R.color.flying_type_foreground),
    Poison(PokemonTypeEnum.Poison, R.color.poison_type_background, R.color.poison_type_foreground),
    Ground(PokemonTypeEnum.Ground, R.color.ground_type_background, R.color.ground_type_foreground),
    Rock(PokemonTypeEnum.Rock, R.color.rock_type_background, R.color.rock_type_foreground),
    Bug(PokemonTypeEnum.Bug, R.color.bug_type_background, R.color.bug_type_foreground),
    Ghost(PokemonTypeEnum.Ghost, R.color.ghost_type_background, R.color.ghost_type_foreground),
    Steel(PokemonTypeEnum.Steel, R.color.steel_type_background, R.color.steel_type_foreground),
    Fire(PokemonTypeEnum.Fire, R.color.fire_type_background, R.color.fire_type_foreground),
    Water(PokemonTypeEnum.Water, R.color.water_type_background, R.color.water_type_foreground),
    Grass(PokemonTypeEnum.Grass, R.color.grass_type_background, R.color.grass_type_foreground),
    Electric(PokemonTypeEnum.Electric, R.color.electric_type_background, R.color.electric_type_foreground),
    Psychic(PokemonTypeEnum.Psychic, R.color.psychic_type_background, R.color.psychic_type_foreground),
    Ice(PokemonTypeEnum.Ice, R.color.ice_type_background, R.color.ice_type_foreground),
    Dragon(PokemonTypeEnum.Dragon, R.color.dragon_type_background, R.color.dragon_type_foreground),
    Dark(PokemonTypeEnum.Dark, R.color.dark_type_background, R.color.dark_type_foreground),
    Fairy(PokemonTypeEnum.Fairy, R.color.fairy_type_background, R.color.fairy_type_foreground),
    Unknown(PokemonTypeEnum.Unknown, R.color.unknown_type_background, R.color.unknown_type_foreground);

    companion object{
        fun fromType(type: String?): PokemonTypeColor? = values().firstOrNull { it.type.id == type }
    }
}