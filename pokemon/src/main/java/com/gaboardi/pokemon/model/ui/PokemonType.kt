package com.gaboardi.pokemon.model.ui

data class PokemonType(
    val slot: Int,
    val name: String
)