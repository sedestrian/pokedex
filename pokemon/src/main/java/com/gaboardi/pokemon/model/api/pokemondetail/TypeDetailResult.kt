package com.gaboardi.pokemon.model.api.pokemondetail

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TypeDetailResult(
    val name: String,
    val url: String
)