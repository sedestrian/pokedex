package com.gaboardi.pokemon.model.ui

import androidx.recyclerview.widget.DiffUtil

data class Pokemon(
    val id: Int,
    val name: String,
    val order: Int,
    val officialArtwork: String?,
    val dreamWorldSvg: String?,
    val speciesName: String,
    val speciesId: Int,
    val types: List<PokemonType>,
    val stats: List<PokemonStat>
){
    companion object{
        val DiffUtil = object: DiffUtil.ItemCallback<Pokemon>() {
            override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
                return oldItem == newItem
            }

            override fun getChangePayload(oldItem: Pokemon, newItem: Pokemon): Any {
                return Differences(
                    if(oldItem.id != newItem.id) newItem.id else null,
                    if(oldItem.name != newItem.name) newItem.name else null,
                    if(oldItem.order != newItem.order) newItem.order else null,
                    if(oldItem.officialArtwork != newItem.officialArtwork) newItem.officialArtwork else null,
                    if(oldItem.dreamWorldSvg != newItem.dreamWorldSvg) newItem.dreamWorldSvg else null,
                    if(oldItem.speciesName != newItem.speciesName) newItem.speciesName else null,
                    if(oldItem.speciesId != newItem.speciesId) newItem.speciesId else null,
                    if (!oldItem.types.containsAll(newItem.types) || !newItem.types.containsAll(oldItem.types))
                        newItem.types else null,
                    if (!oldItem.stats.containsAll(newItem.stats) || !newItem.stats.containsAll(oldItem.stats))
                        newItem.stats else null
                )
            }
        }
    }

    data class Differences(
        val id: Int?,
        val name: String?,
        val order: Int?,
        val officialArtwork: String?,
        val dreamWorldSvg: String?,
        val speciesName: String?,
        val speciesId: Int?,
        val types: List<PokemonType>?,
        val stats: List<PokemonStat>?
    )
}