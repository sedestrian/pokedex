package com.gaboardi.pokemon.model.ui

import androidx.annotation.StringRes
import com.gaboardi.pokemon.R

enum class PokemonStatType(
    val statName: String,
    @StringRes val statRes: Int
) {
    Hp("hp", R.string.hp),
    Attack("attack", R.string.attack),
    Defense("defense", R.string.defense),
    SpAtk("special-attack", R.string.special_attack),
    SpDef("special-defense", R.string.special_defense),
    Speed("speed", R.string.speed),
    Accuracy("accuracy", R.string.accuracy),
    Evasion("evasion", R.string.evasion);

    companion object{
        fun from(name: String?): PokemonStatType? = values().firstOrNull { it.statName == name }
    }
}