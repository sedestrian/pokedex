package com.gaboardi.pokemon.model.api.pokemondetail

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonDetailsResult(
    val id: Int,
    val name: String,
    val order: Int,
    val sprites: SpriteResult,
    val species: SpeciesResult,
    val stats: List<StatResult>,
    val types: List<TypeResult>
)