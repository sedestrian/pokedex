package com.gaboardi.pokemon.model.db

import androidx.room.DatabaseView
import androidx.room.Relation

@DatabaseView("""
    SELECT 
        pokemon.id,
        pokemon.name,
        pokemon.`order`,
        pokemon.officialArtwork,
        pokemon.dreamWorldSvg,
        pokemon.speciesName,
        pokemon.speciesId,
        keys.*,
        types.*,
        stats.*
    FROM 
        pokemon
    LEFT JOIN
        keys ON pokemon.id = keys.pokemonId
    LEFT JOIN 
        types ON pokemon.id = types.pokemonId
    LEFT JOIN
        stats ON pokemon.id = stats.pokemonId
    GROUP BY
        pokemon.id
""")
data class PokemonData(
    val id: Int,
    val name: String,
    val order: Int,
    val officialArtwork: String?,
    val dreamWorldSvg: String?,
    val speciesName: String,
    val speciesId: Int,
    @Relation(parentColumn = "id", entityColumn = "pokemonId")
    val key: RemoteKeyEntity?,
    @Relation(parentColumn = "id", entityColumn = "pokemonId")
    val types: List<TypeEntity>,
    @Relation(parentColumn = "id", entityColumn = "pokemonId")
    val stats: List<StatEntity>
)