package com.gaboardi.pokemon.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon")
data class PokemonEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val order: Int,
    val speciesName: String,
    val speciesId: Int,
    val officialArtwork: String?,
    val dreamWorldSvg: String?
)