package com.gaboardi.pokemon.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.gaboardi.pokemon.model.db.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class PokemonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPokemonEntity(pokemon: PokemonEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPokemonEntity(pokemon: List<PokemonEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPokemonTypes(types: List<TypeEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPokemonStats(stats: List<StatEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertKeys(keys: List<RemoteKeyEntity>)

    @Transaction
    open suspend fun insertPokemon(data: PokemonInsertData) {
        insertPokemonEntity(data.pokemon)
        insertPokemonTypes(data.pokemonTypes)
        insertPokemonStats(data.pokemonStats)
    }

    @Transaction
    open suspend fun insertPokemon(data: List<PokemonInsertData>) {
        insertPokemonEntity(data.map { it.pokemon })
        insertPokemonTypes(data.flatMap { it.pokemonTypes })
        insertPokemonStats(data.flatMap { it.pokemonStats })
    }

    @Query("SELECT * FROM keys WHERE pokemonId = :id LIMIT 1")
    abstract suspend fun getKeyFor(id: Int): RemoteKeyEntity?

    @Transaction
    @Query("SELECT * FROM pokemon ORDER BY id")
    abstract fun observePokemon(): PagingSource<Int, PokemonData>

    @Transaction
    @Query("SELECT * FROM pokemondata WHERE id IN (SELECT pokemonId FROM keys WHERE currentPage BETWEEN :start AND :end)")
    abstract suspend fun getPokemonPage(start: Int, end: Int): List<PokemonData>

    @Query("SELECT COUNT(*) FROM pokemondata WHERE id IN (SELECT pokemonId FROM keys WHERE currentPage < :page)")
    abstract suspend fun getItemsBefore(page: Int): Int

    @Query("SELECT COUNT(*) FROM pokemondata WHERE id IN (SELECT pokemonId FROM keys WHERE currentPage > :page)")
    abstract suspend fun getItemsAfter(page: Int): Int

    @Transaction
    @Query("SELECT * FROM pokemon WHERE id = :pokemonId LIMIT 1")
    abstract fun observePokemon(pokemonId: Int): Flow<PokemonData?>
}