package com.gaboardi.pokemon.di

import androidx.paging.ExperimentalPagingApi
import com.gaboardi.pokemon.datasource.local.PokemonLocalDataSource
import com.gaboardi.pokemon.datasource.local.PokemonLocalDataSourceImpl
import com.gaboardi.pokemon.datasource.remote.PokemonRemoteDataSource
import com.gaboardi.pokemon.datasource.remote.PokemonRemoteDataSourceImpl
import com.gaboardi.pokemon.datasource.remote.PokemonRemoteMediator
import com.gaboardi.pokemon.repository.PokemonRepository
import com.gaboardi.pokemon.repository.PokemonRepositoryImpl
import org.koin.dsl.module

@ExperimentalPagingApi
val pokemonModule = module {
    factory { PokemonRemoteMediator(get(), get()) }
    factory<PokemonRepository> { PokemonRepositoryImpl(get(), get(), get()) }
    factory<PokemonRemoteDataSource> { PokemonRemoteDataSourceImpl(get()) }
    factory<PokemonLocalDataSource> { PokemonLocalDataSourceImpl(get()) }
}