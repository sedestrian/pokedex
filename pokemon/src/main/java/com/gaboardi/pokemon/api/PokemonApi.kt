package com.gaboardi.pokemon.api

import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.api.pokemonlist.PokemonListContainer
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {
    @GET("pokemon")
    suspend fun pokemonList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
    ): PokemonListContainer

    @GET("pokemon/{id}")
    suspend fun getPokemonDetails(
        @Path("id") pokemonId: Int
    ): PokemonDetailsResult
}