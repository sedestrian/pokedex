package com.gaboardi.pokemon.repository

import androidx.paging.*
import com.gaboardi.common.model.State
import com.gaboardi.pokemon.converter.toUi
import com.gaboardi.pokemon.datasource.local.PokemonLocalDataSource
import com.gaboardi.pokemon.datasource.remote.PokemonRemoteDataSource
import com.gaboardi.pokemon.datasource.remote.PokemonRemoteMediator
import com.gaboardi.pokemon.model.ui.Pokemon
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class PokemonRepositoryImpl @ExperimentalPagingApi constructor(
    private val remoteMediator: PokemonRemoteMediator,
    private val localDataSource: PokemonLocalDataSource,
    private val remoteDataSource: PokemonRemoteDataSource
) : PokemonRepository {
    @ExperimentalPagingApi
    override suspend fun getPokemonPagingStream(pagingConfig: PagingConfig): Flow<PagingData<Pokemon>> {
        val pagingSourceFactory = { localDataSource.observePokemonPaging(pagingConfig.pageSize) }

        return Pager(
            config = pagingConfig,
            remoteMediator = remoteMediator,
            pagingSourceFactory = pagingSourceFactory
        ).flow.map { pagingData -> pagingData.map { it.toUi() } }
    }

    override suspend fun downloadPokemon(pokemonId: Int) {
        val localPokemon = localDataSource.observePokemon(pokemonId).first()
        if (localPokemon != null) return
        val pokemon = remoteDataSource.getPokemonData(pokemonId)
        localDataSource.insertPokemon(pokemon)
    }

    override fun downloadPokemonFlow(pokemonId: Int) = flow {
        emit(State.loading())
        try{
            downloadPokemon(pokemonId)
            emit(State.success())
        }catch (e: Exception){
            emit(State.error(e.localizedMessage))
        }
    }

    override fun observePokemon(pokemonId: Int) = localDataSource.observePokemon(pokemonId)
}