package com.gaboardi.pokemon.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.gaboardi.common.model.State
import com.gaboardi.pokemon.model.ui.Pokemon
import kotlinx.coroutines.flow.Flow

interface PokemonRepository {
    fun observePokemon(pokemonId: Int): Flow<Pokemon?>

    @ExperimentalPagingApi
    suspend fun getPokemonPagingStream(pagingConfig: PagingConfig): Flow<PagingData<Pokemon>>

    suspend fun downloadPokemon(pokemonId: Int)
    fun downloadPokemonFlow(pokemonId: Int): Flow<State>
}