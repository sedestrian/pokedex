package com.gaboardi.pokemon.converter

import android.net.Uri
import com.gaboardi.pokemon.model.api.pokemondetail.PokemonDetailsResult
import com.gaboardi.pokemon.model.db.*
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemon.model.ui.PokemonStat
import com.gaboardi.pokemon.model.ui.PokemonType

fun PokemonData.toUi(): Pokemon {
    return Pokemon(
        id,
        name,
        order,
        officialArtwork,
        dreamWorldSvg,
        speciesName,
        speciesId,
        types.map { it.toUi() },
        stats.map { it.toUi() }
    )
}

fun PokemonDetailsResult.toInsertData(): PokemonInsertData {
    val speciesId = Uri.parse(species.url).lastPathSegment?.toInt() ?: 0
    return PokemonInsertData(
        PokemonEntity(
            id,
            name,
            order,
            species.name,
            speciesId,
            sprites.other.officialArtwork.front_default,
            sprites.other.dream_world.front_default
        ),
        stats.map { statEntry ->
            StatEntity(
                0,
                id,
                statEntry.base_stat,
                statEntry.effort,
                statEntry.stat.name
            )
        },
        types.map { typeEntry ->
            TypeEntity(
                0,
                id,
                typeEntry.slot,
                typeEntry.type.name
            )
        }
    )
}

fun StatEntity.toUi(): PokemonStat {
    return PokemonStat(
        baseStat,
        effort,
        name
    )
}

fun TypeEntity.toUi(): PokemonType {
    return PokemonType(
        slot,
        name
    )
}