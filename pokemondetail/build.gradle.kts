import AppDependencies.implementGlide
import AppDependencies.implementKoin
import AppDependencies.implementOrbit

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)

    defaultConfig {
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner(AppConfig.androidTestInstrumentation)
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(AppDependencies.appLibraries)
    implementation(AppDependencies.material)
    implementation(AppDependencies.navigation)
    implementation(AppDependencies.paging)
    implementation(AppDependencies.recyclerView)
    implementation(AppDependencies.lottie)
    implementation(AppDependencies.shimmer)

    implementKoin()
    implementGlide()
    implementOrbit()

    implementModule(AppDependencies.Modules.navigation)
    implementModule(AppDependencies.Modules.common)
    implementModule(AppDependencies.Modules.commonui)
    implementModule(AppDependencies.Modules.pokemon)
    implementModule(AppDependencies.Modules.pokemonui)
    implementModule(AppDependencies.Modules.pokemonSpecies)
    implementModule(AppDependencies.Modules.evolutionChain)
}

repositories {
    maven {
        url = uri("https://jitpack.io")
    }
}