package com.gaboardi.pokemondetail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.paging.ExperimentalPagingApi
import com.gaboardi.evolutionchain.repository.EvolutionRepository
import com.gaboardi.pokemon.repository.PokemonRepository
import com.gaboardi.pokemondetail.mvi.DetailLoadState
import com.gaboardi.pokemondetail.mvi.PokemonDetailSideEffect
import com.gaboardi.pokemondetail.mvi.PokemonDetailState
import com.gaboardi.pokemonspecies.repository.SpeciesRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container

@ExperimentalPagingApi
class PokemonDetailViewModel(
    private val pokemonRepository: PokemonRepository,
    private val speciesRepository: SpeciesRepository,
    private val evolutionsRepository: EvolutionRepository
) : ContainerHost<PokemonDetailState, PokemonDetailSideEffect>, ViewModel() {
    private var pokemonJob: Job? = null
    private var speciesJob: Job? = null
    private var evolutionsJob: Job? = null
    private var pokemonUpdateJob: Job? = null
    private var speciesUpdateJob: Job? = null
    private var evolutionsUpdateJob: Job? = null
    private var evolutionSpeciesUpdateJob: Job? = null

    override val container =
        container<PokemonDetailState, PokemonDetailSideEffect>(PokemonDetailState())

    fun evolutionClicked(pokemonId: Int) = intent {
        state.pokemonDetail?.let { currentPokemon ->
            if (currentPokemon.id != pokemonId)
                postSideEffect(PokemonDetailSideEffect.EvolutionClicked(pokemonId))
        }
    }

    fun getPokemon(pokemonId: Int) = intent {
        reduce { state.copy(loadState = DetailLoadState.PokemonStartedLoading) }
        coroutineScope {
            pokemonJob?.cancel()
            pokemonJob = launch {
                pokemonRepository.observePokemon(pokemonId).collect {
                    reduce { state.copy(pokemonDetail = it) }
                    it?.let { pokemon ->
                        downloadSpecies(pokemon.speciesId)
                    }
                }
            }

            pokemonUpdateJob?.cancel()
            pokemonUpdateJob = launch {
                pokemonRepository.downloadPokemonFlow(pokemonId).collect {
                    reduce { state.copy(pokemonLoadingState = it) }
                }
            }
        }
    }

    fun downloadSpecies(speciesId: Int) = intent {
        reduce { state.copy(loadState = DetailLoadState.SpeciesStartedLoading) }
        coroutineScope {
            speciesJob?.cancel()
            speciesJob = launch {
                speciesRepository.observeSpecies(speciesId).collect {
                    reduce { state.copy(pokemonSpecies = it) }
                    it?.let { species ->
                        downloadEvolutions(species.evolutionChain)
                    }
                }
            }

            speciesUpdateJob?.cancel()
            speciesUpdateJob = launch {
                speciesRepository.downloadSpecies(speciesId).collect {
                    reduce { state.copy(speciesLoadingState = it) }
                }
            }
        }
    }

    fun downloadEvolutions(chainId: Int) = intent {
        reduce { state.copy(loadState = DetailLoadState.EvolutionsStartedLoading) }
        coroutineScope {
            evolutionsJob?.cancel()
            evolutionsJob = launch {
                evolutionsRepository.observeEvolutions(chainId).collect {
                    reduce { state.copy(evolutionChain = it) }
                    it?.let { chain ->
                        val species = chain.evolutions
                            .flatMap { evolution ->
                                listOf(evolution.evolveeId, evolution.evolvedId)
                            }.distinct()
                        downloadEvolutionSpecies(species)
                    }
                }
            }

            evolutionsUpdateJob?.cancel()
            evolutionsUpdateJob = launch {
                evolutionsRepository.loadEvolutions(chainId).collect {
                    reduce { state.copy(chainLoadingState = it) }
                }
            }
        }
    }

    private fun downloadEvolutionSpecies(speciesIds: List<Int>) = intent {
        coroutineScope {
            evolutionSpeciesUpdateJob?.cancel()
            evolutionSpeciesUpdateJob = launch {
                speciesRepository.downloadSpecies(speciesIds).collect {
                    reduce { state.copy(evolutionsLoadingState = it) }
                }
            }
        }
    }
}