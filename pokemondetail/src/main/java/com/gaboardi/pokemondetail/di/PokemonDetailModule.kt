package com.gaboardi.pokemondetail.di

import androidx.paging.ExperimentalPagingApi
import com.gaboardi.pokemondetail.viewmodel.PokemonDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@ExperimentalPagingApi
val pokemonDetailModule = module {
    viewModel { PokemonDetailViewModel(get(), get(), get()) }
}