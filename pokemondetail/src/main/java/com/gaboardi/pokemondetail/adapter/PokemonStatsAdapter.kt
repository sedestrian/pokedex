package com.gaboardi.pokemondetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemon.model.ui.PokemonStat
import com.gaboardi.pokemondetail.databinding.StatItemBinding
import com.gaboardi.pokemondetail.viewholder.PokemonStatsViewHolder

class PokemonStatsAdapter :
    ListAdapter<PokemonStat, PokemonStatsViewHolder>(PokemonStat.DiffUtil) {
    private var color: PokemonColor = PokemonColor.GREEN
        set(value) {
            field = value
            notifyItemRangeChanged(0, itemCount, ColorChangePayload(value))
        }
    private lateinit var inflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonStatsViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = StatItemBinding.inflate(inflater, parent, false)
        return PokemonStatsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PokemonStatsViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, color)
    }

    override fun onBindViewHolder(
        holder: PokemonStatsViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.forEach {
                when (it) {
                    is PokemonStat.ChangePayload -> holder.bindForChanges(it)
                    is ColorChangePayload -> holder.updateColor(it)
                    else -> super.onBindViewHolder(holder, position, payloads)
                }
            }
        }
    }

    fun setColors(color: PokemonColor) {
        if (color != this.color)
            this.color = color
    }

    data class ColorChangePayload(val color: PokemonColor?)
}