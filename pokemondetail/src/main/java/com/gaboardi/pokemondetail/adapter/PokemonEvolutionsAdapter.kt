package com.gaboardi.pokemondetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.gaboardi.evolutionchain.model.ui.Evolution
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemondetail.databinding.EvolutionItemBinding
import com.gaboardi.pokemondetail.viewholder.EvolutionViewHolder
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies

class PokemonEvolutionsAdapter: ListAdapter<Evolution, EvolutionViewHolder>(Evolution.DiffUtil) {
    private lateinit var inflater: LayoutInflater
    private var onEvolutionClicked: ((pokemonId: Int) -> Unit)? = null
    private var onListChangedListener: ((MutableList<Evolution>, MutableList<Evolution>) -> Unit)? =
        null
    private var color: PokemonColor = PokemonColor.GREEN
        set(value) {
            field = value
            notifyItemRangeChanged(0, itemCount, ColorChangePayload(value))
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvolutionViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = EvolutionItemBinding.inflate(inflater, parent, false)
        return EvolutionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EvolutionViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, color){
            onEvolutionClicked?.invoke(it)
        }
    }

    override fun onBindViewHolder(
        holder: EvolutionViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.forEach { payload ->
                when (payload) {
                    is Evolution.ChangePayload -> holder.bindForChanges(payload) { onEvolutionClicked?.invoke(it) }
                    is ColorChangePayload -> holder.updateColor(payload)
                    else -> super.onBindViewHolder(holder, position, payloads)
                }
            }
        }
    }

    fun onEvolutionClicked(listener: ((pokemonId: Int) -> Unit)?){
        onEvolutionClicked = listener
    }

    override fun onCurrentListChanged(
        previousList: MutableList<Evolution>,
        currentList: MutableList<Evolution>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        onListChangedListener?.invoke(previousList, currentList)
    }

    fun onListChanged(listener: (MutableList<Evolution>, MutableList<Evolution>) -> Unit) {
        onListChangedListener = listener
    }

    fun setColors(color: PokemonColor) {
        if (color != this.color)
            this.color = color
    }

    data class ColorChangePayload(val color: PokemonColor?)
}