package com.gaboardi.pokemondetail.mvi

import androidx.paging.PagingData
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.common.model.State
import com.gaboardi.evolutionchain.model.ui.Chain
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies

data class PokemonDetailState(
    val loadState: DetailLoadState = DetailLoadState.Beginning,
    val pokemonDetail: Pokemon? = null,
    val pokemonSpecies: PokemonSpecies? = null,
    val evolutionChain: Chain? = null,
    val pokemonLoadingState: State = State.loading(),
    val speciesLoadingState: State = State.loading(),
    val chainLoadingState: State = State.loading(),
    val evolutionsLoadingState: State = State.loading()
)

sealed class DetailLoadState(val index: Int) {
    object Beginning : DetailLoadState(0)
    object PokemonStartedLoading: DetailLoadState(1)
    object SpeciesStartedLoading : DetailLoadState(2)
    object EvolutionsStartedLoading : DetailLoadState(3)

    operator fun compareTo(other: DetailLoadState): Int {
        return index.compareTo(other.index)
    }

    operator fun compareTo(other: Int): Int {
        return index.compareTo(other)
    }
}