package com.gaboardi.pokemondetail.mvi

sealed class PokemonDetailSideEffect{
    data class EvolutionClicked(val pokemonId: Int): PokemonDetailSideEffect()
}