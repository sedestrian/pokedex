package com.gaboardi.pokemondetail.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.extensions.isUsingNightModeResources

class PokemonShadowView(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private val shadowPaint: Paint = Paint().apply {
        color = Color.TRANSPARENT
        val night = context.isUsingNightModeResources()
        val color = if (night) Color.parseColor("#50FFFFFF") else Color.parseColor("#50000000")
        setShadowLayer(40f, 0f, 0f, color)
    }

    private val shadowPadding = 18.dp

    private val shadowRect: RectF = RectF()

    override fun onLayout(
        changed: Boolean,
        leftLayout: Int,
        topLayout: Int,
        rightLayout: Int,
        bottomLayout: Int
    ) {
        super.onLayout(changed, leftLayout, topLayout, rightLayout, bottomLayout)
        shadowRect.apply {
            left = shadowPadding.toFloat()
            top = shadowPadding.toFloat()
            right = width.toFloat() - shadowPadding
            bottom = height.toFloat() - shadowPadding
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawOval(shadowRect, shadowPaint)
    }
}