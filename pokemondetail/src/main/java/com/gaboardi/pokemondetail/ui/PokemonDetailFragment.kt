package com.gaboardi.pokemondetail.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.gaboardi.common.extensions.dp
import com.gaboardi.common.model.State
import com.gaboardi.commonui.glide.GlideApp
import com.gaboardi.commonui.itemdecoration.GridSpacingItemDecorator
import com.gaboardi.commonui.itemdecoration.LinearSpacingItemDecorator
import com.gaboardi.commonui.view.ProgressTimeLatch
import com.gaboardi.evolutionchain.model.ui.Chain
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemondetail.R
import com.gaboardi.pokemondetail.adapter.PokemonEvolutionsAdapter
import com.gaboardi.pokemondetail.adapter.PokemonStatsAdapter
import com.gaboardi.pokemondetail.databinding.FragmentPokemonDetailBinding
import com.gaboardi.pokemonui.extensions.setPokemonType
import com.gaboardi.pokemondetail.mvi.DetailLoadState.*
import com.gaboardi.pokemondetail.mvi.PokemonDetailSideEffect
import com.gaboardi.pokemondetail.mvi.PokemonDetailState
import com.gaboardi.pokemondetail.viewmodel.PokemonDetailViewModel
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

@ExperimentalPagingApi
class PokemonDetailFragment : Fragment() {
    private lateinit var binding: FragmentPokemonDetailBinding
    private lateinit var statsAdapter: PokemonStatsAdapter
    private lateinit var evolutionsAdapter: PokemonEvolutionsAdapter
    private lateinit var imageLoadingLatch: ProgressTimeLatch
    private lateinit var funFactLatch: ProgressTimeLatch
    private lateinit var evolutionsLatch: ProgressTimeLatch

    private var funFactInitialized = false

    private val args: PokemonDetailFragmentArgs by navArgs()
    private val viewModel: PokemonDetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokemonDetailBinding.inflate(inflater, container, false)
        setupLatches()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getPokemon(args.pokemonId)

        imageLoadingLatch.refreshing = true
        funFactLatch.refreshing = true
        evolutionsLatch.refreshing = true

        setupStatsRecycler()
        setupEvolutionsRecycler()

        lifecycleScope.launchWhenStarted {
            viewModel.container.stateFlow.collect {
                renderState(it)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.container.sideEffectFlow.collect { sideEffect ->
                when (sideEffect) {
                    is PokemonDetailSideEffect.EvolutionClicked -> navigateToEvolution(sideEffect.pokemonId)
                }
            }
        }
    }

    private fun renderState(state: PokemonDetailState) {
//        TransitionManager.beginDelayedTransition(binding.root)
        renderPokemon(state)
        renderSpecies(state)
        renderChain(state)
    }

    private fun renderPokemon(state: PokemonDetailState) {
        if (state.loadState >= PokemonStartedLoading) {
            state.pokemonDetail?.let { pokemon ->
                showPokemon(pokemon)
            } ?: showPokemonError()
        }
    }

    private fun showPokemon(pokemon: Pokemon) {
        binding.nestedScrollView.isVisible = true
        binding.pokemonMissing.isVisible = false

        val image = pokemon.dreamWorldSvg ?: pokemon.officialArtwork
        GlideApp
            .with(this)
            .load(image ?: R.drawable.missing)
            .listener(imageLoadedListener)
            .error(R.drawable.missing)
            .into(binding.pokemonImage)

        binding.name.text = pokemon.speciesName.capitalize(Locale.getDefault())

        setChipColors(pokemon)

        statsAdapter.submitList(pokemon.stats)

        val pokemonColor = PokemonColor.from(pokemon.types.first().name)
        pokemonColor?.let {
            statsAdapter.setColors(it)
            evolutionsAdapter.setColors(it)
        }
    }

    private fun showPokemonError() {
        binding.nestedScrollView.isVisible = false
        binding.pokemonMissing.isVisible = true
    }

    private fun renderSpecies(state: PokemonDetailState) {
        if (state.loadState >= SpeciesStartedLoading) {
            funFactLatch.refreshing = state.speciesLoadingState is State.Loading
            if (state.speciesLoadingState is State.Error) showFunFactError(state.pokemonDetail?.speciesId)
            if (!funFactInitialized)
                state.pokemonSpecies?.let { species ->
                    showSpecies(species)
                }
        }
    }

    private fun showSpecies(species: PokemonSpecies) {
        val currentLanguage = Locale.getDefault().language
        val languageFacts = species.funFacts.filter { it.language == currentLanguage }

        binding.funFact.text = languageFacts.random().factText
        funFactInitialized = true
    }

    private fun renderChain(state: PokemonDetailState) {
        if (state.loadState >= EvolutionsStartedLoading) {
            evolutionsLatch.refreshing = state.chainLoadingState is State.Loading
            if (state.chainLoadingState is State.Error) showChainError(state.pokemonSpecies?.evolutionChain)
            else state.evolutionChain?.let { chain ->
                showChain(chain, state.pokemonDetail, state)
            }
        }
    }

    private fun showFunFactError(speciesId: Int?) {
        binding.funFact.isVisible = false
        binding.funFactError.isVisible = true
        binding.factErrorButton.setOnClickListener {
            speciesId?.let {
                funFactLatch.refreshing = true
                binding.funFactError.isVisible = false
                viewModel.downloadSpecies(speciesId)
            }
        }
    }

    private fun showChainError(chainId: Int?) {
        binding.evolutionsRecycler.isVisible = false
        binding.chainError.isVisible = true
        binding.chainErrorButton.setOnClickListener {
            chainId?.let {
                evolutionsLatch.refreshing = true
                binding.chainError.isVisible = false
                viewModel.downloadEvolutions(chainId)
            }
        }
    }

    private fun showChain(chain: Chain, pokemon: Pokemon?, state: PokemonDetailState) {
        binding.noEvolutions.isVisible =
            chain.evolutions.isEmpty() && state.chainLoadingState !is State.Loading
        if (chain.evolutions.isNotEmpty()) {
            evolutionsAdapter.submitList(chain.evolutions)
        }
        val pokemonColor = PokemonColor.from(pokemon?.types?.first()?.name)
        pokemonColor?.let {
            evolutionsAdapter.setColors(it)
        }
    }

    private fun setChipColors(pokemon: Pokemon) {
        val firstType = pokemon.types.firstOrNull()
        val secondType = pokemon.types.getOrNull(1)

        binding.firstType.isGone = firstType == null
        firstType?.let {
            binding.firstType.setPokemonType(it)
        }

        binding.secondType.isVisible = secondType != null
        secondType?.let {
            binding.secondType.setPokemonType(it)
        }
    }

    private val imageLoadedListener = object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            imageLoadingLatch.refreshing = false
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            imageLoadingLatch.refreshing = false
            return false
        }
    }

    private fun navigateToEvolution(pokemonId: Int) {
        funFactInitialized = false
        val action = PokemonDetailFragmentDirections.actionPokemonDetailFragmentSelf(pokemonId)
        findNavController().navigate(action)
    }

    private fun setupStatsRecycler() {
        statsAdapter = PokemonStatsAdapter()
        binding.statsRecycler.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.statsRecycler.addItemDecoration(GridSpacingItemDecorator(2, 8.dp, 8.dp))
        binding.statsRecycler.adapter = statsAdapter
    }

    private fun setupLatches() {
        imageLoadingLatch = ProgressTimeLatch {
            binding.imageLoading.isVisible = it
            binding.pokemonImage.isInvisible = it
        }
        funFactLatch = ProgressTimeLatch {
            binding.shimmerFrameLayout.isVisible = it
            binding.funFact.isVisible = !it
        }
        evolutionsLatch = ProgressTimeLatch {
            binding.evolutionShimmer.isVisible = it
        }
    }

    private fun setupEvolutionsRecycler() {
        evolutionsAdapter = PokemonEvolutionsAdapter()
        binding.evolutionsRecycler.addItemDecoration(LinearSpacingItemDecorator(direction = LinearLayoutManager.HORIZONTAL))
        binding.evolutionsRecycler.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        evolutionsAdapter.onListChanged { oldList, newList ->
            binding.evolutionShimmer.isVisible = newList.size <= 0
            binding.evolutionsRecycler.isVisible = newList.size > 0
        }
        evolutionsAdapter.onEvolutionClicked {
            viewModel.evolutionClicked(it)
        }
        binding.evolutionsRecycler.adapter = evolutionsAdapter
    }
}