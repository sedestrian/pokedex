package com.gaboardi.pokemondetail.viewholder

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.gaboardi.commonui.glide.GlideApp
import com.gaboardi.evolutionchain.model.ui.Evolution
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemondetail.R
import com.gaboardi.pokemondetail.adapter.PokemonEvolutionsAdapter
import com.gaboardi.pokemondetail.databinding.EvolutionItemBinding


class EvolutionViewHolder(private val binding: EvolutionItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        evolution: Evolution,
        color: PokemonColor,
        onEvolutionClicked: ((pokemonId: Int) -> Unit)?
    ) {
        evolution.evolvee?.variety?.let { evolvee ->
            val evolveePicture = evolvee.dreamWorldSvg ?: evolvee.officialArtwork
            binding.evolvee.setOnClickListener { onEvolutionClicked?.invoke(evolvee.id) }
            loadImageIn(binding.evolvee, binding.evolveeLoading, evolveePicture)
        } ?: run {
            binding.evolveeLoading.isVisible = true
            binding.evolvee.isInvisible = true
        }
        evolution.evolved?.variety?.let { evolved ->
            val evolvedPicture = evolved.dreamWorldSvg ?: evolved.officialArtwork
            binding.evolved.setOnClickListener { onEvolutionClicked?.invoke(evolved.id) }
            loadImageIn(binding.evolved, binding.evolvedLoading, evolvedPicture)
        } ?: run {
            binding.evolvedLoading.isVisible = true
            binding.evolved.isInvisible = true
        }

        binding.level.isVisible = evolution.minLevel != null || evolution.minHappiness != null
        binding.happinessIcon.isVisible = evolution.minHappiness != null
        binding.heldItem.isVisible = evolution.needsItem

        val levelString = if (evolution.minLevel != null)
            binding.root.context.getString(R.string.evolution_level_label, evolution.minLevel)
        else if (evolution.minHappiness != null)
            evolution.minHappiness.toString()
        else ""

        binding.level.text = levelString

        if (binding.card.cardBackgroundColor.defaultColor != color.background)
            setColor(color)
    }

    fun bindForChanges(
        changes: Evolution.ChangePayload,
        onEvolutionClicked: ((pokemonId: Int) -> Unit)?
    ) {
        if (changes.minLevel != null) binding.level.text =
            binding.root.context.getString(R.string.evolution_level_label, changes.minLevel)

        changes.evolvee?.let { evolvee ->
            val evolveePicture = evolvee.variety?.dreamWorldSvg
                ?: evolvee.variety?.officialArtwork
            binding.evolvee.setOnClickListener { onEvolutionClicked?.invoke(evolvee.id) }
            loadImageIn(binding.evolvee, binding.evolveeLoading, evolveePicture)
        }

        changes.evolved?.let { evolved ->
            val evolvedPicture =
                evolved.variety?.dreamWorldSvg ?: evolved.variety?.officialArtwork
            binding.evolved.setOnClickListener { onEvolutionClicked?.invoke(evolved.id) }
            loadImageIn(binding.evolved, binding.evolvedLoading, evolvedPicture)
        }
    }

    private fun loadImageIn(imageView: ImageView, placeholder: View, picture: String?) {
        GlideApp.with(binding.root)
            .load(picture ?: R.drawable.missing)
            .error(R.drawable.missing)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    placeholder.isVisible = false
                    imageView.isVisible = true
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    placeholder.isVisible = false
                    imageView.isVisible = true
                    return false
                }
            })
            .into(imageView)
    }

    fun updateColor(colorChangePayload: PokemonEvolutionsAdapter.ColorChangePayload) {
        colorChangePayload.color?.let { pokemonColor ->
            setColor(pokemonColor)
        }
    }

    private fun setColor(pokemonColor: PokemonColor) {
        val background = ContextCompat.getColor(binding.root.context, pokemonColor.background)
        binding.card.setCardBackgroundColor(background)
    }
}