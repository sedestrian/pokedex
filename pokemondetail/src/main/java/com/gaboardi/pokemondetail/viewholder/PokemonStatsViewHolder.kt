package com.gaboardi.pokemondetail.viewholder

import android.os.Build
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemon.model.ui.PokemonStat
import com.gaboardi.pokemon.model.ui.PokemonStatType
import com.gaboardi.pokemondetail.R
import com.gaboardi.pokemondetail.adapter.PokemonStatsAdapter
import com.gaboardi.pokemondetail.databinding.StatItemBinding

class PokemonStatsViewHolder(private val binding: StatItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(pokemonStat: PokemonStat, pokemonColor: PokemonColor) {
        binding.amount.text = pokemonStat.baseStat.toString()

        val statName = PokemonStatType.from(pokemonStat.name)?.statRes ?: R.string.question_marks
        binding.name.setText(statName)

        binding.statProgress.progress = pokemonStat.baseStat

        val background = ContextCompat.getColor(binding.root.context, pokemonColor.background)
        val foreground = ContextCompat.getColor(binding.root.context, pokemonColor.foreground)
        binding.card.setCardBackgroundColor(background)
        binding.statProgress.setIndicatorColor(foreground)
    }

    fun bindForChanges(
        changes: PokemonStat.ChangePayload
    ) {
        if (changes.name != null) binding.name.text = changes.name

        changes.baseStat?.let {
            binding.amount.text = changes.baseStat.toString()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                binding.statProgress.setProgress(it, true)
            else
                binding.statProgress.progress = it
        }
    }

    fun updateColor(colorChangePayload: PokemonStatsAdapter.ColorChangePayload) {
        colorChangePayload.color?.let { pokemonColor ->
            val background = ContextCompat.getColor(binding.root.context, pokemonColor.background)
            val foreground = ContextCompat.getColor(binding.root.context, pokemonColor.foreground)

            binding.card.setCardBackgroundColor(background)
            binding.statProgress.setIndicatorColor(foreground)
        }
    }
}