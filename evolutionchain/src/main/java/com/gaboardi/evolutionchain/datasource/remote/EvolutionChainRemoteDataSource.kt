package com.gaboardi.evolutionchain.datasource.remote

import com.gaboardi.evolutionchain.model.api.EvolutionChainResult

interface EvolutionChainRemoteDataSource {
    suspend fun getEvolutionChain(chainId: Int): EvolutionChainResult
}