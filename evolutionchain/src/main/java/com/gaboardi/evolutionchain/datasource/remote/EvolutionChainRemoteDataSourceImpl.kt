package com.gaboardi.evolutionchain.datasource.remote

import com.gaboardi.evolutionchain.api.PokemonEvolutionApi
import com.gaboardi.evolutionchain.model.api.EvolutionChainResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

class EvolutionChainRemoteDataSourceImpl(
    private val retrofit: Retrofit
) : EvolutionChainRemoteDataSource {
    override suspend fun getEvolutionChain(chainId: Int): EvolutionChainResult =
        withContext(Dispatchers.IO) {
            val api = retrofit.create(PokemonEvolutionApi::class.java)
            api.getEvolutionChain(chainId)
        }
}