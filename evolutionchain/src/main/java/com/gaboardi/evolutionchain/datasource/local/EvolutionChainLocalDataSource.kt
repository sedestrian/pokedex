package com.gaboardi.evolutionchain.datasource.local

import com.gaboardi.evolutionchain.model.api.EvolutionChainResult
import com.gaboardi.evolutionchain.model.ui.Chain
import kotlinx.coroutines.flow.Flow

interface EvolutionChainLocalDataSource {
    fun observeEvolutions(chainId: Int): Flow<Chain?>
    suspend fun insertEvolutions(evolutions: EvolutionChainResult)
}