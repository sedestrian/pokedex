package com.gaboardi.evolutionchain.datasource.local

import com.gaboardi.evolutionchain.converter.toInsertData
import com.gaboardi.evolutionchain.converter.toUi
import com.gaboardi.evolutionchain.dao.EvolutionDao
import com.gaboardi.evolutionchain.model.api.EvolutionChainResult
import com.gaboardi.evolutionchain.model.ui.Chain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class EvolutionChainLocalDataSourceImpl(
    private val evolutionDao: EvolutionDao
) : EvolutionChainLocalDataSource {
    override fun observeEvolutions(chainId: Int) =
        evolutionDao.observeEvolutions(chainId)
            .distinctUntilChanged()
            .map { Chain(chainId, it.map { item -> item.toUi() }) }

    override suspend fun insertEvolutions(evolutions: EvolutionChainResult) =
        withContext(Dispatchers.IO) {
            evolutionDao.insertEvolutions(evolutions.toInsertData())
        }
}