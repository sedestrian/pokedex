package com.gaboardi.evolutionchain.dao

import androidx.room.*
import com.gaboardi.evolutionchain.model.db.EvolutionData
import com.gaboardi.evolutionchain.model.db.EvolutionEntity
import com.gaboardi.evolutionchain.model.db.EvolutionInsertData
import kotlinx.coroutines.flow.Flow

@Dao
abstract class EvolutionDao {
    @Transaction
    open fun insertEvolutions(evolutionData: EvolutionInsertData) {
        insertEvolutions(evolutionData.evolutions)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEvolutions(evolutions: List<EvolutionEntity>)

    @Query("SELECT * FROM evolutions WHERE chainId = :chainId ORDER BY evolveeId")
    abstract fun observeEvolutions(chainId: Int): Flow<List<EvolutionData>>
}