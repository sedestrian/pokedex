package com.gaboardi.evolutionchain.api

import com.gaboardi.evolutionchain.model.api.EvolutionChainResult
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonEvolutionApi {
    @GET("evolution-chain/{chainId}")
    suspend fun getEvolutionChain(@Path("chainId") chainId: Int): EvolutionChainResult
}