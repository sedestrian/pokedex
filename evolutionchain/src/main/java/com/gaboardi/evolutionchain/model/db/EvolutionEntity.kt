package com.gaboardi.evolutionchain.model.db

import androidx.room.Entity

@Entity(tableName = "evolutions", primaryKeys = ["evolveeId", "evolvedId"])
data class EvolutionEntity(
    val evolveeId: Int,
    val evolvedId: Int,
    val minLevel: Int?,
    val needsItem: Boolean,
    val minHappiness: Int?,
    val chainId: Int
)