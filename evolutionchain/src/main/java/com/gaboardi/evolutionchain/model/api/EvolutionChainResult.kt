package com.gaboardi.evolutionchain.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EvolutionChainResult(
    val id: Int,
    val chain: EvolutionResult
)
