package com.gaboardi.evolutionchain.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EvolutionDetailsResult(
    val min_level: Int?,
    val held_item: HeldItemResult?,
    val item: HeldItemResult?,
    val min_happiness: Int?
)