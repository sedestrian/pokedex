package com.gaboardi.evolutionchain.model.ui

import androidx.recyclerview.widget.DiffUtil
import com.gaboardi.pokemonspecies.model.ui.PokemonSpecies

data class Evolution(
    val evolveeId: Int,
    val evolvedId: Int,
    val minLevel: Int?,
    val needsItem: Boolean,
    val minHappiness: Int?,
    val evolvee: PokemonSpecies?,
    val evolved: PokemonSpecies?
) {
    data class ChangePayload(
        val minLevel: Int?,
        val needsItem: Boolean?,
        val evolvee: PokemonSpecies?,
        val evolved: PokemonSpecies?
    )

    companion object {
        val DiffUtil = object : DiffUtil.ItemCallback<Evolution>() {
            override fun areItemsTheSame(oldItem: Evolution, newItem: Evolution): Boolean {
                return true
            }

            override fun areContentsTheSame(oldItem: Evolution, newItem: Evolution): Boolean {
                val isSameEvolvee =
                    oldItem.evolvee?.variety?.id == newItem.evolvee?.variety?.id
                val isSameEvolved =
                    oldItem.evolved?.variety?.id == newItem.evolved?.variety?.id
                return oldItem.minLevel == newItem.minLevel &&
                        oldItem.needsItem == newItem.needsItem &&
                        isSameEvolvee &&
                        isSameEvolved
            }

            override fun getChangePayload(oldItem: Evolution, newItem: Evolution): Any? {
                val isSameMinLevel = oldItem.minLevel == newItem.minLevel
                val isSameNeedsItem = oldItem.needsItem == newItem.needsItem
                val isSameEvolveePicture =
                    getPicture(oldItem.evolvee) == getPicture(newItem.evolvee)
                val isSameEvolvedPicture =
                    getPicture(oldItem.evolved) == getPicture(newItem.evolved)

                if (isSameMinLevel &&
                    isSameNeedsItem &&
                    isSameEvolveePicture &&
                    isSameEvolvedPicture
                )
                    return null

                return ChangePayload(
                    if (!isSameMinLevel) newItem.minLevel else null,
                    if (!isSameNeedsItem) newItem.needsItem else null,
                    if (!isSameEvolveePicture) newItem.evolvee else null,
                    if (!isSameEvolvedPicture) newItem.evolved else null,
                )
            }

            private fun getPicture(species: PokemonSpecies?): String? {
                val pokemon = species?.variety
                return if (pokemon?.dreamWorldSvg != null) pokemon.dreamWorldSvg else pokemon?.officialArtwork
            }
        }
    }
}