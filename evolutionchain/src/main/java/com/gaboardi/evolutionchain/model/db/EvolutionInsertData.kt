package com.gaboardi.evolutionchain.model.db

data class EvolutionInsertData(
    val evolutions: List<EvolutionEntity>
)