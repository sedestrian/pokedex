package com.gaboardi.evolutionchain.model.ui

data class Chain(
    val id: Int,
    val evolutions: List<Evolution>
)