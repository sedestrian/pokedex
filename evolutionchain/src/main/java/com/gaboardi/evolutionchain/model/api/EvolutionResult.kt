package com.gaboardi.evolutionchain.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EvolutionResult(
    val evolves_to: List<EvolutionResult?>,
    val species: EvolutionSpeciesResult,
    val evolution_details: List<EvolutionDetailsResult>
)
