package com.gaboardi.evolutionchain.model.db

import androidx.room.Embedded
import androidx.room.Relation
import com.gaboardi.pokemonspecies.model.db.PokemonSpeciesData

data class EvolutionData(
    @Embedded
    val evolution: EvolutionEntity,
    @Relation(parentColumn = "evolveeId", entityColumn = "id")
    val evolvee: PokemonSpeciesData?,
    @Relation(parentColumn = "evolvedId", entityColumn = "id")
    val evolved: PokemonSpeciesData?
)