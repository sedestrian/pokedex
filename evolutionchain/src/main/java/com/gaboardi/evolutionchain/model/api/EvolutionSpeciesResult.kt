package com.gaboardi.evolutionchain.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EvolutionSpeciesResult(
    val name: String,
    val url: String
)
