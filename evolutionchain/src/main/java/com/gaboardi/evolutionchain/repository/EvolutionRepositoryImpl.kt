package com.gaboardi.evolutionchain.repository

import com.gaboardi.common.model.State
import com.gaboardi.evolutionchain.datasource.local.EvolutionChainLocalDataSource
import com.gaboardi.evolutionchain.datasource.remote.EvolutionChainRemoteDataSource
import com.gaboardi.evolutionchain.model.ui.Chain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class EvolutionRepositoryImpl(
    private val remoteDataSource: EvolutionChainRemoteDataSource,
    private val localDataSource: EvolutionChainLocalDataSource
) : EvolutionRepository {
    override fun observeEvolutions(chainId: Int): Flow<Chain?> =
        localDataSource.observeEvolutions(chainId)

    override fun loadEvolutions(chainId: Int) = flow {
        emit(State.loading())
        try {
            val data = remoteDataSource.getEvolutionChain(chainId)
            localDataSource.insertEvolutions(data)
            emit(State.success())
        } catch (e: Exception) {
            emit(State.error(e.localizedMessage))
        }
    }
}