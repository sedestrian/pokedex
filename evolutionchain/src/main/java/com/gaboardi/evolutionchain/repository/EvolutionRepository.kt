package com.gaboardi.evolutionchain.repository

import com.gaboardi.common.model.State
import com.gaboardi.evolutionchain.model.ui.Chain
import kotlinx.coroutines.flow.Flow

interface EvolutionRepository {
    fun loadEvolutions(chainId: Int): Flow<State>
    fun observeEvolutions(chainId: Int): Flow<Chain?>
}