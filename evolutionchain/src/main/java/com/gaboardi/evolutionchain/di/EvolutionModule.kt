package com.gaboardi.evolutionchain.di

import com.gaboardi.evolutionchain.datasource.local.EvolutionChainLocalDataSource
import com.gaboardi.evolutionchain.datasource.local.EvolutionChainLocalDataSourceImpl
import com.gaboardi.evolutionchain.datasource.remote.EvolutionChainRemoteDataSource
import com.gaboardi.evolutionchain.datasource.remote.EvolutionChainRemoteDataSourceImpl
import com.gaboardi.evolutionchain.repository.EvolutionRepository
import com.gaboardi.evolutionchain.repository.EvolutionRepositoryImpl
import org.koin.dsl.module

val evolutionModule = module {
    single<EvolutionChainLocalDataSource> { EvolutionChainLocalDataSourceImpl(get()) }
    single<EvolutionChainRemoteDataSource> { EvolutionChainRemoteDataSourceImpl(get()) }
    single<EvolutionRepository> { EvolutionRepositoryImpl(get(), get()) }
}