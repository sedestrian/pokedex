package com.gaboardi.evolutionchain.converter

import android.net.Uri
import com.gaboardi.evolutionchain.model.api.EvolutionChainResult
import com.gaboardi.evolutionchain.model.api.EvolutionResult
import com.gaboardi.evolutionchain.model.db.EvolutionData
import com.gaboardi.evolutionchain.model.db.EvolutionEntity
import com.gaboardi.evolutionchain.model.db.EvolutionInsertData
import com.gaboardi.evolutionchain.model.ui.Evolution
import com.gaboardi.pokemonspecies.converter.toUi

fun EvolutionChainResult.toInsertData(): EvolutionInsertData {
    return EvolutionInsertData(
        chain.toEvolutions(id)
    )
}

fun EvolutionData.toUi(): Evolution {
    return Evolution(
        evolution.evolveeId,
        evolution.evolvedId,
        evolution.minLevel,
        evolution.needsItem,
        evolution.minHappiness,
        evolvee?.toUi(),
        evolved?.toUi()
    )
}

fun EvolutionResult.toEvolutions(chainId: Int): List<EvolutionEntity> {
    val evolveeId: Int = Uri.parse(species.url).lastPathSegment?.toInt() ?: 0
    return getEntities(this, evolveeId, chainId)
}

private fun getEntities(
    result: EvolutionResult?,
    evolveeId: Int,
    chainId: Int
): List<EvolutionEntity> {
    return result?.evolves_to?.flatMap {
        val evolvedId: Int = Uri.parse(it?.species?.url).lastPathSegment?.toInt() ?: 0
        val needsItem = it?.evolution_details?.firstOrNull()?.let { details ->
            details.held_item != null || details.item != null
        }
        getEntities(it, evolvedId, chainId) +
                EvolutionEntity(
                    evolveeId,
                    evolvedId,
                    it?.evolution_details?.firstOrNull()?.min_level,
                    needsItem ?: false,
                    it?.evolution_details?.firstOrNull()?.min_happiness,
                    chainId
                )
    } ?: listOf()
}