package com.gaboardi.pokemonlist.mvi

import androidx.paging.PagingData
import com.gaboardi.common.model.ErrorMessage
import com.gaboardi.pokemon.model.ui.Pokemon

data class PokemonListState(
    val pokemon: PagingData<Pokemon> = PagingData.empty(),
    val loading: Boolean = false,
    val error: ErrorMessage? = null
)