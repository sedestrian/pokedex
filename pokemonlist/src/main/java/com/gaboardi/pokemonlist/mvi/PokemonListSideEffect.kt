package com.gaboardi.pokemonlist.mvi

sealed class PokemonListSideEffect {
    data class NavigateToPokemonDetail(val pokemonId: Int): PokemonListSideEffect()
}