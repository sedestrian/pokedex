package com.gaboardi.pokemonlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemonlist.databinding.PokemonItemBinding
import com.gaboardi.pokemonlist.viewholder.PokemonViewHolder

class PokemonAdapter(private val onClick: (Pokemon) -> Unit) :
    PagingDataAdapter<Pokemon, PokemonViewHolder>(Pokemon.DiffUtil) {
    private lateinit var inflater: LayoutInflater

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, onClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = PokemonItemBinding.inflate(inflater, parent, false)
        return PokemonViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: PokemonViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.forEach {
                when (it) {
                    is Pokemon.Differences -> holder.bindForChanges(it)
                    else -> super.onBindViewHolder(holder, position, payloads)
                }
            }
        }
    }
}