package com.gaboardi.pokemonlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.gaboardi.pokemonlist.databinding.LoadStateItemBinding
import com.gaboardi.pokemonlist.viewholder.PokemonLoadStateViewHolder

class PokemonLoadStateAdapter(private val onRetry: () -> Unit) :
    LoadStateAdapter<PokemonLoadStateViewHolder>() {
    private lateinit var inflater: LayoutInflater

    override fun onBindViewHolder(holder: PokemonLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState, onRetry)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): PokemonLoadStateViewHolder {
        if (!this::inflater.isInitialized) inflater = LayoutInflater.from(parent.context)
        val binding = LoadStateItemBinding.inflate(inflater, parent, false)
        return PokemonLoadStateViewHolder(binding)
    }
}