package com.gaboardi.pokemonlist.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemon.repository.PokemonRepository
import com.gaboardi.pokemonlist.mvi.PokemonListSideEffect
import com.gaboardi.pokemonlist.mvi.PokemonListState
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.coroutines.transformFlow
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.strict.orbit
import org.orbitmvi.orbit.syntax.strict.reduce
import org.orbitmvi.orbit.viewmodel.container

@ExperimentalPagingApi
class PokemonListViewModel(
    private val pokemonListRepository: PokemonRepository
) : ContainerHost<PokemonListState, PokemonListSideEffect>, ViewModel() {
    companion object {
        private const val PageSize = 20
    }

    override val container = container<PokemonListState, PokemonListSideEffect>(PokemonListState())

    init {
        loadPokemonList()
    }

    private fun loadPokemonList() = orbit {
        transformFlow {
            val config = PagingConfig(
                pageSize = PageSize,
                enablePlaceholders = false
            )
            pokemonListRepository.getPokemonPagingStream(config).cachedIn(viewModelScope)
        }.reduce {
            state.copy(pokemon = event)
        }
    }

    fun userSelectedPokemon(pokemon: Pokemon) = intent {
        postSideEffect(PokemonListSideEffect.NavigateToPokemonDetail(pokemon.id))
    }
}