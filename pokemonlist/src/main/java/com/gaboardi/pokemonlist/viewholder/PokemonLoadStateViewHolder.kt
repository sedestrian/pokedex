package com.gaboardi.pokemonlist.viewholder

import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.pokemonlist.databinding.LoadStateItemBinding

class PokemonLoadStateViewHolder(private val binding: LoadStateItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(loadState: LoadState, onRetry: () -> Unit) {
        when(loadState){
            is LoadState.NotLoading -> {
                binding.errorGroup.isVisible = false
                binding.loading.isVisible = true
            }
            LoadState.Loading -> {
                binding.errorGroup.isVisible = false
                binding.loading.isVisible = true
            }
            is LoadState.Error -> {
                binding.errorGroup.isVisible = true
                binding.loading.isVisible = false
            }
        }
        binding.loadErrorButton.setOnClickListener {
            onRetry()
        }
    }
}