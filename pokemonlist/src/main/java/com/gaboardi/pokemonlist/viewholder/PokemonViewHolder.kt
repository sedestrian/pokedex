package com.gaboardi.pokemonlist.viewholder

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.gaboardi.commonui.extensions.isUsingNightModeResources
import com.gaboardi.commonui.glide.GlideApp
import com.gaboardi.pokemon.model.ui.Pokemon
import com.gaboardi.pokemon.model.ui.PokemonColor
import com.gaboardi.pokemon.model.ui.PokemonType
import com.gaboardi.pokemonui.extensions.setPokemonType
import com.gaboardi.pokemonlist.R
import com.gaboardi.pokemonlist.databinding.PokemonItemBinding
import java.util.*

class PokemonViewHolder(private val binding: PokemonItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(pokemon: Pokemon?, onClick: (Pokemon) -> Unit) {
        pokemon?.let {
            binding.name.text = it.speciesName.capitalize(Locale.getDefault())
            binding.number.text =
                binding.number.context.getString(R.string.pokemon_id_format, it.id)

            setCardBackgroundColor(it.types)
            setChipColors(it.types)

            val image = it.dreamWorldSvg ?: it.officialArtwork
            setImage(binding.imageView, image)

            binding.card.setOnClickListener { _ ->
                onClick(it)
            }
        }
    }

    fun bindForChanges(changes: Pokemon.Differences) {
        changes.speciesName?.let { binding.name.text = it.capitalize(Locale.getDefault()) }

        if (changes.id != null) binding.number.text =
            binding.number.context.getString(R.string.pokemon_id_format, changes.id)

        if (changes.dreamWorldSvg != null || changes.officialArtwork != null) {
            val image = changes.dreamWorldSvg ?: changes.officialArtwork

            setImage(binding.imageView, image)
        }

        changes.types?.let {
            setChipColors(it)
            setCardBackgroundColor(it)
        }
    }

    private fun setImage(imageView: ImageView, image: String?) {
        GlideApp
            .with(imageView)
            .load(image)
            .error(R.drawable.missing)
            .into(imageView)
    }

    private fun setChipColors(pokemonTypes: List<PokemonType>) {
        val firstType = pokemonTypes.firstOrNull()
        val secondType = pokemonTypes.getOrNull(1)
        showFirstType(firstType)
        showSecondType(secondType)
    }

    private fun showFirstType(pokemonType: PokemonType?) {
        binding.pokemonTypeBottom.isVisible = pokemonType != null
        pokemonType?.let { type ->
            binding.pokemonTypeBottom.setPokemonType(type, false)
        }
        /*if (isNight) {
            val typeColors = PokemonTypeColor.fromType(pokemonType.name)
            typeColors?.let {
                val background = ContextCompat.getColor(binding.card.context, typeColors.background)
                val foreground = ContextCompat.getColor(binding.card.context, typeColors.textColor)

                binding.pokemonTypeBottom.chipBackgroundColor = ColorStateList.valueOf(background)
                binding.pokemonTypeBottom.setTextColor(foreground)
            }
        } else {
            val background = binding.card.context.getColorAttribute(R.attr.colorSurface)
            val foreground = binding.card.context.getColorAttribute(R.attr.colorOnSurface)
            binding.pokemonTypeBottom.chipBackgroundColor = ColorStateList.valueOf(background)
            binding.pokemonTypeBottom.setTextColor(foreground)
        }
        binding.pokemonTypeBottom.text = pokemonType.name.capitalize(Locale.getDefault())*/
    }

    private fun showSecondType(pokemonType: PokemonType?) {
        binding.pokemonTypeTop.isVisible = pokemonType != null
        pokemonType?.let { type ->
            binding.pokemonTypeTop.setPokemonType(type, false)
        }
        /*if (isNight) {
            val typeColors = PokemonTypeColor.fromType(pokemonType.name)
            typeColors?.let {
                val background = ContextCompat.getColor(binding.card.context, typeColors.background)
                val foreground = ContextCompat.getColor(binding.card.context, typeColors.textColor)

                binding.pokemonTypeTop.chipBackgroundColor = ColorStateList.valueOf(background)
                binding.pokemonTypeTop.setTextColor(foreground)
            }
        } else {
            val background = binding.card.context.getColorAttribute(R.attr.colorSurface)
            val foreground = binding.card.context.getColorAttribute(R.attr.colorOnSurface)
            binding.pokemonTypeTop.chipBackgroundColor = ColorStateList.valueOf(background)
            binding.pokemonTypeTop.setTextColor(foreground)
        }
        binding.pokemonTypeTop.text = pokemonType.name.capitalize(Locale.getDefault())*/
    }

    private fun setCardBackgroundColor(pokemonTypes: List<PokemonType>) {
        val nightMode = binding.root.context.isUsingNightModeResources()
        val colorRes = if (nightMode)
            R.color.darkCard
        else
            PokemonColor.from(pokemonTypes.firstOrNull()?.name)?.background ?: R.color.green_200

        val color = ContextCompat.getColor(
            binding.card.context,
            colorRes
        )
        binding.card.setCardBackgroundColor(color)
    }
}