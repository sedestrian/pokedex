package com.gaboardi.pokemonlist.di

import androidx.paging.ExperimentalPagingApi
import com.gaboardi.pokemonlist.viewmodel.PokemonListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@ExperimentalPagingApi
val pokemonListModule = module {
    viewModel { PokemonListViewModel(get()) }
}