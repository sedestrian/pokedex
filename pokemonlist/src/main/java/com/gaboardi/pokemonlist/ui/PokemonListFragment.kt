package com.gaboardi.pokemonlist.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.gaboardi.common.extensions.dp
import com.gaboardi.commonui.itemdecoration.GridSpacingItemDecorator
import com.gaboardi.pokemonlist.adapter.PokemonAdapter
import com.gaboardi.pokemonlist.adapter.PokemonLoadStateAdapter
import com.gaboardi.pokemonlist.databinding.FragmentPokemonListBinding
import com.gaboardi.pokemonlist.mvi.PokemonListSideEffect
import com.gaboardi.pokemonlist.mvi.PokemonListState
import com.gaboardi.pokemonlist.viewmodel.PokemonListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class PokemonListFragment : Fragment() {
    private lateinit var binding: FragmentPokemonListBinding
    private lateinit var adapter: PokemonAdapter

    @ExperimentalPagingApi
    private val viewModel: PokemonListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokemonListBinding.inflate(inflater, container, false)
        return binding.root
    }

    @ExperimentalPagingApi
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()
        listenToEvents()

        lifecycleScope.launchWhenCreated {
            viewModel.container.stateFlow.collect { render(it) }
        }
        lifecycleScope.launchWhenCreated {
            viewModel.container.sideEffectFlow.collect { reactToSideEffect(it) }
        }
    }

    private fun listenToEvents() {
        binding.refresh.setOnRefreshListener {
            adapter.refresh()
        }
    }

    private suspend fun render(state: PokemonListState) {
        adapter.submitData(state.pokemon)
    }

    private fun reactToSideEffect(sideEffect: PokemonListSideEffect) {
        when (sideEffect) {
            is PokemonListSideEffect.NavigateToPokemonDetail -> navigateToDetail(sideEffect.pokemonId)
        }
    }

    private fun navigateToDetail(pokemonId: Int) {
        val action =
            PokemonListFragmentDirections.actionPokemonListFragmentToPokemonDetailFlow(pokemonId)
        findNavController().navigate(action)
    }

    @ExperimentalPagingApi
    private fun setupRecycler() {
        adapter = PokemonAdapter { pokemon ->
            viewModel.userSelectedPokemon(pokemon)
        }

        adapter.addLoadStateListener { loadState ->
            handleLoadingError(loadState)
//            handleErrorMessage(loadState)
        }

        val layoutManager = GridLayoutManager(requireContext(), 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val type = binding.pokemonRecycler.adapter?.getItemViewType(position)
                return if (type == 1) 2 else 1
            }
        }

        binding.pokemonRecycler.layoutManager = layoutManager
        binding.pokemonRecycler.addItemDecoration(GridSpacingItemDecorator(2, 8.dp, 8.dp))
        binding.pokemonRecycler.adapter = adapter.withLoadStateFooter(PokemonLoadStateAdapter {
            adapter.retry()
        })
    }

    /*private fun handleErrorMessage(loadState: CombinedLoadStates){
        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error
        errorState?.let {
            Snackbar.make(
                binding.coordinator,
                R.string.server_error,
                Snackbar.LENGTH_INDEFINITE
            ).show()
        }
    }*/

    private fun handleLoadingError(loadState: CombinedLoadStates) {
        if (
            loadState.refresh !is LoadState.Loading &&
            ((loadState.append.endOfPaginationReached &&
                    loadState.prepend.endOfPaginationReached) ||
                    loadState.refresh is LoadState.Error) &&
            loadState.mediator != null &&
            adapter.itemCount < 1
        ) {
            binding.pokemonRecycler.isVisible = false
            binding.loadingView.isVisible = false
            binding.refresh.isRefreshing = false
            binding.emptyView.isVisible = true
        } else if (loadState.refresh !is LoadState.Loading && adapter.itemCount > 0) {
            binding.pokemonRecycler.isVisible = true
            binding.emptyView.isVisible = false
            binding.loadingView.isVisible = false
            binding.refresh.isRefreshing = false
        } else {
            binding.pokemonRecycler.isVisible = false
            binding.emptyView.isVisible = false
            binding.loadingView.isVisible = true
        }
    }
}